import sqlalchemy as sa
from modelbase import Base
import sqlalchemy.orm as orm
from Models.user import User
from Models.division import Division


class Company(Base):
    __tablename__ = "Companies"

    CompanyID = sa.Column(sa.Integer, primary_key=True)
    CompanyName = sa.Column(sa.String)

    division = orm.relation("Division")

    __table_args__ = {"schema": "security"}

    def __repr__(self):
        return (
            f"<COMPANY(CompanyID='{self.CompanyID}', CompanyName='{self.CompanyName}')>"
        )
