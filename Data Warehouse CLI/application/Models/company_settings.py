import sqlalchemy as sa
from modelbase import Base
import sqlalchemy.orm as orm
from Models.template import Template


class CompanySettings(Base):
    __tablename__ = "CompanySettings"
    __table_args__ = {"schema": "security"}

    CompanySettingID = sa.Column(sa.Integer, primary_key=True)
    SettingDescription = sa.Column(sa.String)

    templates = orm.relation("Template", back_populates="company_settings")

    def __repr__(self):
        return f"{self.CompanySettingID} - {self.SettingDescription}"
