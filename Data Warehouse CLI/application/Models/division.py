import sqlalchemy as sa
from modelbase import Base
import sqlalchemy.orm as orm
from Models.user import User


class Division(Base):
    __tablename__ = "Divisions"

    DivisionID = sa.Column(sa.Integer, primary_key=True)
    CompanyID = sa.Column(sa.Integer, sa.ForeignKey("security.Companies.CompanyID"))
    Division = sa.Column(sa.String)
    DivisionName = sa.Column(sa.String)

    user = orm.relation("User")
    company = orm.relation("Company")

    __table_args__ = {"schema": "security"}

    def __repr__(self):
        return f"<DIVSION(DivisionID='{self.DivisionID}', CompanyID='{self.CompanyID}', Division='{self.Division}', DivisionName='{self.DivisionName}')>"
