import sqlalchemy as sa
from modelbase import Base
import sqlalchemy.orm as orm


class Schema(Base):
    __tablename__ = "Schemas"

    SchemaID = sa.Column(sa.Integer, primary_key=True)
    SchemaName = sa.Column(sa.String)
    SchemaDescription = sa.Column(sa.Column)
    DatabaseID = sa.Column(sa.Integer, sa.ForeignKey("Databases.DatabaseID"))

    def __repr__(self):
        return f"{self.SchemaID} - {self.SchemaName}"
