import sqlalchemy as sa
from modelbase import Base
import sqlalchemy.orm as orm



class Table(Base):
    __tablename__ = "Tables"

    TableID = sa.Column(sa.Integer, primary_key=True)
    TableName = sa.Column(sa.String)
    TableDescription = sa.Column(sa.String)
    TablePrefix = sa.Column(sa.String)
    StreamID = sa.Column(sa.Integer, sa.ForeignKey("Streams.StreamID"))
    DestinationSchemaID = sa.Column(sa.Integer, sa.ForeignKey("Schemas.SchemaID"))
    Sequence = sa.Column(sa.Integer)
    HasHash = sa.Column(sa.Binary)
    
    def __repr__(self):
        return f'{self.TableID} - {self.TableName}'

