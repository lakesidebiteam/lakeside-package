import sqlalchemy as sa
from modelbase import Base
import sqlalchemy.orm as orm


class Schema(Base):
    __tablename__ = "Schemas"
    __table_args__ = {"schema": "security"}

    SchemaID = sa.Column(sa.Integer, primary_key=True)
    SchemaName = sa.Column(sa.String)

    def __repr__(self):
        return f"{self.SchemaID} - {self.SchemaName}"
