import sqlalchemy as sa
from modelbase import Base
import sqlalchemy.orm as orm
from Models.user import User


class Template(Base):
    __tablename__ = "Templates"
    __table_args__ = {"schema": "security"}

    TemplateID = sa.Column(sa.Integer, primary_key=True)
    TemplateName = sa.Column(sa.String)
    CompanySettingsID = sa.Column(
        sa.Integer, sa.ForeignKey("security.CompanySettings.CompanySettingID")
    )

    users = orm.relation("User", back_populates="template")
    company_settings = orm.relation("CompanySettings")

    def __repr__(self):
        return f"{self.TemplateID} - {self.TemplateName}"
