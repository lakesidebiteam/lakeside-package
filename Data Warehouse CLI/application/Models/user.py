import sqlalchemy as sa
from modelbase import Base
import sqlalchemy.orm as orm
from sqlalchemy.sql import func


class User(Base):
    __tablename__ = "Users"

    UserLogin = sa.Column(sa.String, primary_key=True)
    DivisionID = sa.Column(sa.Integer, sa.ForeignKey("security.Divisions.DivisionID"))
    TemplateID = sa.Column(sa.Integer, sa.ForeignKey("security.Templates.TemplateID"))
    CreatedDate = sa.Column(sa.DateTime, server_default=func.now())

    template = orm.relation("Template")
    division = orm.relationship("Division")

    __table_args__ = ({"schema": "security"},)

    def __repr__(self):
        return f"<USER(UserLogin='{self.UserLogin}')>"
