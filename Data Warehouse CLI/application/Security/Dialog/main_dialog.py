import Security.Dialog.user_dialog as u
import Security.Dialog.template_dialog as t
import Security.Dialog.permissions_dialog as p
from Security.security import deploy_security
from Utilities.printing import print_help


def security_options():
    print()
    chosen_settings = input("What settings would you like to change? ")

    if chosen_settings.upper() in ["L", "LIST", "H", "HELP"]:
        print()
        print("You can use the following commands.")
        print()
        print_help("Help", "Display help text", "H")
        print_help("Users", "View or modify Users", "U")
        print_help("Templates", "View or Modify Templates", "T")
        print_help("Template Permissions", "Define Template permission by Schema", "P")
        print_help("Deploy Security", "Publishes security settings to the Data Warehouse", "D")
        print_help("Back", "Back to main program", "B")
        print()
    elif chosen_settings.upper() in ["B", "BACK"]:
        return
    elif chosen_settings.upper() in ["U", "USERS"]:
        u.user_options()
    elif chosen_settings.upper() in ["T", "TEMPLATES"]:
        t.template_options()
    elif chosen_settings.casefold() in ["p", "template permissions"]:
        p.permissions_options()
    elif chosen_settings.casefold() in ["d", "deploy", "deploy security"]:
        deploy_security()

    security_options()
