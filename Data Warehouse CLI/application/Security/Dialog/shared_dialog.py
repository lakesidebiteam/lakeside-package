import click
from Models.division import Division
from Models.company import Company
from db_session import create_session
import Security.security as sec


def division(company_id: int, default: str = "000") -> int:
    print()
    division_id = click.prompt("Enter the Division Number: ", default="000")

    if division_id.upper() == "L":
        sec.list_divisions(company_id)
        return division(company_id)

    if division_id.upper() in ["X", "B"]:
        return

    session = create_session()

    div_id = (
        session.query(Division)
        .filter(Division.CompanyID == company_id)
        .filter(Division.Division == division_id)
        .first()
    )

    if not div_id:
        print(
            f"Division {division_id} does not exist for the specified Company ({company_id})."
        )
        return division(company_id)

    return div_id.DivisionID


def company(default: str = "1") -> int:
    print()
    default = str(default)
    company_id = click.prompt("Enter the Company Number: ", type=str, default=default)

    if company_id.upper() == "L":
        sec.list_companies()
        return company()

    if company_id.upper() == "X":
        return

    if not company_id.isdigit():
        print(f"{company_id} is not an integer.")
        return company()

    session = create_session()

    company_check = (
        session.query(Company).filter(Company.CompanyID == int(company_id)).first()
    )

    if not company_check:
        print(f"Company {company_id} does not exist.")
        return company()

    return int(company_id)
