from db_session import create_session
from Models.template import Template
from Models.company_settings import CompanySettings
import Security.security as sec
import click


def template_options():
    print()
    chosen_option = input(
        "Would you like to (C)reate, (U)pdate, (D)elete, (V)iew or go (B)ack? "
    )

    if chosen_option.upper() in ["V", "VIEW"]:
        print()
        sec.list_templates()
    elif chosen_option.upper() in ["B", "BACK", "X", "EXIT"]:
        return
    elif chosen_option.upper() in ["C", "CREATE"]:
        create_new_template()
    elif chosen_option.upper() in ["D", "DELETE"]:
        delete_template()
    else:
        print(f"{chosen_option} is not a recognized command.")

    template_options()


def delete_template():
    print()
    chosen_template, template_name = get_template_name(new=False)

    if not chosen_template:
        return

    if click.confirm(f"Are you sure you want to delete Template: {template_name}?"):
        sec.delete_template(chosen_template)


def create_new_template():

    template_name = get_template_name()

    if not template_name:
        return

    settings_id = get_template_company_settings()

    if not settings_id:
        return

    sec.create_template(template_name, settings_id)


def get_template_company_settings():
    print()
    company_settings = input("Enter the company settings option: ")

    if company_settings.upper() in ["L", "H"]:
        sec.list_settings()
        return get_template_company_settings()

    if company_settings.upper() in ["X", "B"]:
        return

    session = create_session()

    settings_id = (
        session.query(CompanySettings)
        .filter(CompanySettings.SettingDescription == company_settings)
        .first()
    )

    if not settings_id:
        print(f'Company Settings "{company_settings}" does not exist.')
        return get_template_company_settings()

    return settings_id.CompanySettingID


def get_template_name(new=True):
    print()

    if new:
        template_name = input("Enter the new template name: ")
    else:
        template_name = input("Enter the template name: ")

    if template_name.upper() == "L":
        sec.list_templates()
        return get_template_name()

    if template_name.upper() in ["X", "B"]:
        return

    session = create_session()

    temp_id = (
        session.query(Template).filter(Template.TemplateName == template_name).first()
    )

    if temp_id and new:
        print(f"Template {template_name} already exists.")
        return get_template_name()
    elif not temp_id and not new:
        print(f"Template {template_name} already exists.")
        return get_template_name()

    if new:
        return template_name
    else:
        return (temp_id.TemplateID, temp_id.TemplateName)
