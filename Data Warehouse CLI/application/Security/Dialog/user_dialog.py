from db_session import create_session
from Models.user import User
from Models.template import Template
import Security.Dialog.shared_dialog as sd
import Security.security as sec
import click
from sqlalchemy.sql.expression import func


def user_options():
    print()
    chosen_option = input(
        "Would you like to (C)reate, (U)pdate, (D)elete, (V)iew or go (B)ack? "
    )

    if chosen_option.upper() in ["V", "VIEW"]:
        print()
        sec.list_users()
    elif chosen_option.upper() in ["B", "BACK", "X", "EXIT"]:
        return
    elif chosen_option.upper() in ["C", "CREATE"]:
        create_new_user()
    elif chosen_option.upper() in ["D", "DELETE"]:
        delete_user()
    elif chosen_option.upper() in ["U", "UPDATE"]:
        update_user()

    user_options()


def update_user():
    print()
    user_login = input("Which user would you like to update? ")

    if user_login.upper() == "L":
        sec.list_users()
        update_user()
    elif user_login.upper() in ["B", "X"]:
        return
    elif user_login[0] == "\\":
        user_login = "LAKESIDEIND" + user_login
    session = create_session()
    user_id = session.query(User).filter(User.UserLogin == user_login).first()

    if not user_id:
        print(f"User: {user_login} does not exist.")
        update_user()
        return

    company_id = sd.company(str(user_id.division.CompanyID))

    if not company_id:
        return

    if company_id == user_id.division.CompanyID:
        default_div = user_id.division.Division
    else:
        default_div = None

    division_id = sd.division(company_id, default_div)

    if not division_id:
        return

    template_id = user_template(user_id.template.TemplateName)

    if not template_id:
        return

    sec.update_user(user_login, division_id, template_id)

    print(f"User {user_login} successfully updated.")


def delete_user():
    print()
    chosen_user = input("What user would you like to delete? ")

    if chosen_user.upper() == "L":
        sec.list_users()
        delete_user()
    elif chosen_user.upper() in ["B", "X"]:
        return
    elif chosen_user[0] == "\\":
        chosen_user = "LAKESIDEIND" + chosen_user

    session = create_session()

    user_id = session.query(User).filter(User.UserLogin == chosen_user).first()

    if not user_id:
        print(f"User: {chosen_user} does not exist.")
        delete_user()
    else:
        if click.confirm(f"Are you sure you want to delete {chosen_user}?"):
            sec.del_user(chosen_user)
            print(f"User {chosen_user} successfully deleted.")


def create_new_user():
    print()

    new_user = new_user_username()

    if not new_user:
        return

    company_id = sd.company()

    if not company_id:
        return

    division_id = sd.division(company_id)

    if not division_id:
        return

    session = create_session()

    last_user_time = session.query(func.max(User.CreatedDate))

    last_user = session.query(User).filter(User.CreatedDate == last_user_time).first()

    template_id = user_template(last_user.template.TemplateName)

    if not template_id:
        return

    sec.create_user(new_user, division_id, template_id)

    print()
    print(f"User {new_user} successfully created.")


def user_template(default: str = None):
    print()
    default = str(default)

    template_id = click.prompt(
        "Enter the Template Name: ", type=str, default=default, show_default=default
    )

    if template_id.upper() == "L":
        sec.list_templates()
        return user_template(default)

    if template_id.upper() in ["X", "B"]:
        return

    session = create_session()

    temp_id = (
        session.query(Template).filter(Template.TemplateName == template_id).first()
    )

    if not temp_id:
        print(f"Template {template_id} does not exist.")
        return user_template(default)

    return temp_id.TemplateID


def new_user_username():
    print()
    new_user = input("What is the user login? ")

    if new_user.upper() == "X":
        return
    elif new_user[0] == "\\":
        new_user = "LAKESIDEIND" + new_user

    session = create_session()

    user = session.query(User).filter(User.UserLogin == new_user).first()

    if user:
        print(f"User: {new_user} already exists")
        return new_user_username()

    return new_user
