import Security.security as sec
from Security.user import User
from Security.company import Company
from Security.division import Division
from Security.template import Template
from Security.company_settings import CompanySettings
from db_session import create_session
from Utilities.printing import print_help
import click


def new_user_username():
    print()
    new_user = input("What is the user login? ")

    if new_user.upper() == "X":
        return
    elif new_user[0] == "\\":
        new_user = "LAKESIDEIND" + new_user

    session = create_session()

    user = session.query(User).filter(User.UserLogin == new_user).first()

    if user:
        print(f"User: {new_user} already exists")
        return new_user_username()

    return new_user


def company(default: str = "1") -> int:
    print()
    default = str(default)
    company_id = click.prompt("Enter the Company Number: ", type=str, default=default)

    if company_id.upper() == "L":
        sec.list_companies()
        return company()

    if company_id.upper() == "X":
        return

    if not company_id.isdigit():
        print(f"{company_id} is not an integer.")
        return company()

    session = create_session()

    company_check = (
        session.query(Company).filter(Company.CompanyID == int(company_id)).first()
    )

    if not company_check:
        print(f"Company {company_id} does not exist.")
        return company()

    return int(company_id)


def division(company_id: int, default: str = "000") -> int:
    print()
    division_id = click.prompt("Enter the Division Number: ", default="000")

    if division_id.upper() == "L":
        sec.list_divisions(company_id)
        return division(company_id)

    if division_id.upper() in ["X", "B"]:
        return

    session = create_session()

    div_id = (
        session.query(Division)
        .filter(Division.CompanyID == company_id)
        .filter(Division.Division == division_id)
        .first()
    )

    if not div_id:
        print(
            f"Company {division_id} does not exist for the specified Company ({company_id})."
        )
        return division(company_id)

    return div_id.DivisionID


def user_template(default: str = None):
    print()
    default = str(default)

    template_id = click.prompt(
        "Enter the Template Name: ", type=str, default=default, show_default=default
    )

    if template_id.upper() == "L":
        sec.list_templates()
        return user_template(default)

    if template_id.upper() in ["X", "B"]:
        return

    session = create_session()

    temp_id = (
        session.query(Template).filter(Template.TemplateName == template_id).first()
    )

    if not temp_id:
        print(f"Template {division_id} does not exist.")
        return user_template(default)

    return temp_id.TemplateID


def create_new_user():
    print()

    new_user = new_user_username()

    if not new_user:
        return

    company_id = company()

    if not company_id:
        return

    division_id = division(company_id)

    if not division_id:
        return

    template_id = user_template()

    if not template_id:
        return

    sec.create_user(new_user, division_id, template_id)

    print()
    print(f"User {new_user} successfully created.")


def delete_user():
    print()
    chosen_user = input("What user would you like to delete? ")

    if chosen_user.upper() == "L":
        sec.list_users()
        delete_user()
    elif chosen_user.upper() in ["B", "X"]:
        return
    elif chosen_user[0] == "\\":
        chosen_user = "LAKESIDEIND" + chosen_user

    session = create_session()

    user_id = session.query(User).filter(User.UserLogin == chosen_user).first()

    if not user_id:
        print(f"User: {chosen_user} does not exist.")
        delete_user()
    else:
        if click.confirm(f"Are you sure you want to delete {chosen_user}?"):
            sec.del_user(chosen_user)
            print(f"User {chosen_user} successfully deleted.")


def update_user():
    print()
    user_login = input("Which user would you like to update? ")

    if user_login.upper() == "L":
        sec.list_users()
        update_user()
    elif user_login.upper() in ["B", "X"]:
        return
    elif user_login[0] == "\\":
        user_login = "LAKESIDEIND" + user_login
    session = create_session()
    user_id = session.query(User).filter(User.UserLogin == user_login).first()

    if not user_id:
        print(f"User: {chosen_user} does not exist.")
        update_user()
        return

    company_id = company(str(user_id.division.CompanyID))

    if not company_id:
        return

    if company_id == user_id.division.CompanyID:
        default_div = user_id.division.Division
    else:
        default_div = None

    division_id = division(company_id, default_div)

    if not division_id:
        return

    template_id = user_template(user_id.template.TemplateName)

    if not template_id:
        return

    sec.update_user(user_login, division_id, template_id)

    print(f"User {user_login} successfully updated.")


def user_options():
    print()
    chosen_option = input(
        "Would you like to (C)reate, (U)pdate, (D)elete, (V)iew or go (B)ack? "
    )

    if chosen_option.upper() in ["V", "View"]:
        print()
        sec.list_users()
    elif chosen_option.upper() in ["B", "Back"]:
        return
    elif chosen_option.upper() in ["C", "Create"]:
        create_new_user()
    elif chosen_option.upper() in ["D", "Delete"]:
        delete_user()
    elif chosen_option.upper() in ["U", "Update"]:
        update_user()

    user_options()


def get_template_name():
    print()
    template_name = input("Enter the new template name: ")

    if template_name.upper() == "L":
        sec.list_templates()
        return get_template_name()

    if template_name.upper() in ["X", "B"]:
        return

    session = create_session()

    temp_id = (
        session.query(Template).filter(Template.TemplateName == template_name).first()
    )

    if temp_id:
        print(f"Template {template_name} already exists.")
        return get_template_name()

    return template_name


def get_template_company_settings():
    print()
    company_settings = input("Enter the company settings option: ")

    if company_settings.upper() == "L":
        sec.list_settings()
        return get_template_company_settings()

    if company_settings.upper() in ["X", "B"]:
        return

    session = create_session()

    settings_id = (
        session.query(CompanySettings)
        .filter(CompanySettings.SettingDescription == company_settings)
        .first()
    )

    if not settings_id:
        print(f'Company Settings "{company_settings}" does not exist.')
        return get_template_company_settings()

    return settings_id.CompanySettingID


def create_new_template():

    template_name = get_template_name()

    if not template_name:
        return

    settings_id = get_template_company_settings()

    if not settings_id:
        return

    sec.create_template(template_name, settings_id)


def template_options():
    print()
    chosen_option = input(
        "Would you like to (C)reate, (U)pdate, (D)elete, (V)iew or go (B)ack? "
    )

    if chosen_option.upper() in ["V", "View"]:
        print()
        sec.list_templates()
        template_options()
    elif chosen_option.upper() in ["B", "Back"]:
        return
    elif chosen_option.upper() in ["C", "Create"]:
        create_new_template()
        template_options()


def security_options():
    print()
    chosen_settings = input("What settings would you like to change? ")

    if chosen_settings.upper() in ["L", "LIST", "H", "HELP"]:
        print()
        print("You can use the following commands.")
        print()
        print_help("Help", "Display help text", "H")
        print_help("Users", "View or modify Users", "U")
        print_help("Templates", "View or Modify Templates", "T")
        print_help("Back", "Back to main program", "B")
        print()
        security_options()
    elif chosen_settings.upper() in ["B", "Back"]:
        return
    elif chosen_settings.upper() in ["U", "Users"]:
        user_options()
        security_options()
    elif chosen_settings.upper() in ["T", "Templates"]:
        template_options()
        security_options()
