import sqlalchemy.orm as orm
from db_session import create_session
from Models.schema import Schema
from Models.company_settings import CompanySettings
from Models.template import Template
from Models.user import User
from Models.company import Company
from Models.division import Division
from Utilities.printing import print_table


def list_schemas():
    session = create_session()
    schemas = session.query(Schema).all()

    headers = ["ID", "Name"]
    rows = []

    for schema in schemas:
        row = [schema.SchemaID, schema.SchemaName]
        rows.append(row)

    print_table(headers, rows)


def list_companies():
    session = create_session()
    companies = session.query(Company).all()

    headers = ["ID", "Name"]
    rows = []

    for company in companies:
        row = [company.CompanyID, company.CompanyName]
        rows.append(row)

    print_table(headers, rows)


def list_divisions(company_id=None):
    session = create_session()
    divisions = session.query(Division)

    if company_id:
        divisions = divisions.filter(Division.CompanyID == company_id)

    divisions = divisions.all()

    headers = ["Company ID", "Division", "Name"]
    rows = []

    for division in divisions:
        row = [division.CompanyID, division.Division, division.DivisionName]
        rows.append(row)

    print_table(headers, rows)


def list_settings():
    session = create_session()
    settings = session.query(CompanySettings).all()

    headers = ["ID", "Description"]
    rows = []

    for setting in settings:
        row = [setting.CompanySettingID, setting.SettingDescription]
        rows.append(row)

    print_table(headers, rows)


def list_templates():
    session = create_session()
    templates = session.query(Template).all()

    headers = ["ID", "Description", "Company Settings"]
    rows = []

    for template in templates:
        row = [
            template.TemplateID,
            template.TemplateName,
            template.company_settings.SettingDescription,
        ]
        rows.append(row)

    print_table(headers, rows)


def list_users(user_login=None, company=None, template=None):
    session = create_session()

    res = session.query(User)

    if user_login:
        res = res.filter(User.UserLogin == user_login)
    if company:
        res = res.join(User.division, aliased=True).filter_by(CompanyID=company)
    if template:
        res = res.join(User.template, aliased=True).filter_by(TemplateName=template)

    users = res.all()

    headers = ["Login", "Company", "Division", "Template"]
    rows = []

    for user in users:
        template = ""
        company = ""
        division = ""
        if user.template:
            template = user.template.TemplateName
        if user.division:
            division = f"{user.division.Division} - {user.division.DivisionName}"
            if user.division.company:
                company = (
                    f"{user.division.CompanyID} - {user.division.company.CompanyName}"
                )

        row = [user.UserLogin, company, division, template]
        rows.append(row)

    print_table(headers, rows)


def del_user(user_login: str):
    session = create_session()
    session.query(User).filter(User.UserLogin == user_login).delete()
    session.commit()


def create_user(user_login: str, div_id: int, template_id: int):
    session = create_session()
    new_user = User(UserLogin=user_login, DivisionID=div_id, TemplateID=template_id)
    session.add(new_user)
    session.commit()


def update_user(user_login: str, div_id: int = None, template_id: int = None):
    session = create_session()
    user = session.query(User).filter(User.UserLogin == user_login).first()

    if div_id:
        user.DivisionID = div_id
    if template_id:
        user.TemplateID = template_id
    session.commit()


def create_template(template_name: str, company_settings: int):
    session = create_session()
    new_template = Template(
        TemplateName=template_name, CompanySettingsID=company_settings
    )
    session.add(new_template)
    session.commit()


def delete_template(template_id: int):
    session = create_session()
    users = session.query(User).filter(User.TemplateID == template_id).all()

    for user in users:
        user.TemplateID = None

    session.commit()

    session.query(Template).filter(Template.TemplateID == template_id).delete()
    session.commit()


def deploy_security():
    session = create_session()

    session.execute("security.pDeploySecurity")

    session.commit()

    print()
    print("Security Deployed")
    print()

