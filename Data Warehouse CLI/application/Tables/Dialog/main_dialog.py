from Utilities.printing import print_help
import Tables.reload_table as rt


def table_options():
    print()
    selected_option = input("Would you like to (R)eload tables? ")

    if selected_option.casefold() in ["l", "list", "h", "help"]:
        print()
        print("You can use the following commands.")
        print()
        print_help("Help", "Display help text", "H")
        print_help("Reload", "Reload individual tables", "R")
        print_help("Back", "Back to main program", "B")
        print()
    elif selected_option.casefold() in ["b", "back", "x", "exit"]:
        return
    elif selected_option.casefold() in ["r", "reload"]:
        rt.reload_table()
