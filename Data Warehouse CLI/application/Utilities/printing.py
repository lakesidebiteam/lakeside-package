from tabulate import tabulate


def print_table(headers: list, rows: list):
    print(tabulate(rows, headers=headers, tablefmt="github"))


def print_help(command, description, alternate_command=None, cmd_len=30):
    spaces = " " * cmd_len

    if alternate_command:
        command = command + " (" + alternate_command + ")"

    command = command + spaces

    command = command[:cmd_len] + description

    print(command)
