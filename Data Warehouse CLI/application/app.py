import Security.Dialog.main_dialog as sec_d
from pyfiglet import Figlet
from Utilities.printing import print_help

f = Figlet(font="doom")

print(f.renderText("Data Warehouse"))


def main():
    f = Figlet(font="ogre")
    print()
    chosen_module = input("What module would you like to use? ")

    if chosen_module.upper() in ["H", "HELP"]:
        print()
        print("You can use the following commands.")
        print()
        print_help("Help", "Display help text", "H")
        print_help("Security", "View or modify Data Warehouse security", "S")
        print_help("Tables", "Create or reload tables", "T")
        print_help("Exit", "Exit application", "X")
        print()
        main()
    elif chosen_module.upper() in ["X", "EXIT"]:
        return
    elif chosen_module.upper() in ["S", "SECURITY"]:
        print(f.renderText("Security Settings"))
        sec_d.security_options()
        main()


if __name__ == "__main__":
    main()
