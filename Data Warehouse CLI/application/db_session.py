import sqlalchemy as sa
import urllib
import sqlalchemy.orm as orm


def create_session():
    params = urllib.parse.quote_plus(
        "Driver={SQL Server};Server=DWSQLSRV;Database=DW_Metadata;Trusted_Connection=Yes"
    )
    engine = sa.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    Session = orm.sessionmaker(bind=engine)
    return Session()
