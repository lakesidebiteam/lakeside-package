import pypyodbc
import urllib
from sqlalchemy import create_engine, types, event

class LakesideConnections:

    def __init__(self, app=None) -> None:
        self.server_types = ["SQL Server", "PostgreSQL"]
        self.app = "Python" if app is None else app

    def __create_connection_string(
        self, server_type: str, server: str, database: str, username: str=None, password: str=None, port: int=None
    ):

        servers = {
            "SQL Server": {
                "Driver": "SQL Server",
                "Server": server,
                "Database": database,
                "Trusted_Connection": "No" if username else "Yes",
                "Username": username,
                "Password": password,
                "APP": self.app,
            },
            "PostgreSQL": {
                "Driver": "{PostgreSQL ANSI}",
                "Server": server,
                "Port": port,
                "Database": database,
                "Uid": username,
                "Pwd": password,
            },
        }

        if server_type not in servers.keys():
            raise Exception(
                f"server_type: {server_type} is not a valid server type. Use one of the following types: {servers.keys().join(', ')}"
            )

        if username and not password:
            raise Exception("Password required if username is given.")

        conn_string = ""

        for key, value in servers[server_type].items():
            if value:
                conn_string += f"{key}={value};"

        return conn_string

    def create_sql_connection(
        self,
        default=None,
        server=None,
        database=None,
        server_type="SQL Server",
        username=None,
        password=None,
        port=None,
    ) -> pypyodbc.Connection:
        """Creates a pypyodbc connection to SQL Sever

        Parameters
        ----------
        default: str
            Optional: Select a default server (SourceData, AutomatedTasks, DW, DW_Metadata, DW_Stage, ML, Vista, VP Attachments, Tableau)
        server : str
            If no default selected, set the server name
        database : str
            If no default selected, set the database name
        server_type: str
            If no default selected set the server type


        Returns
        -------
        connection
            If connection type = 'sql' then pypyodbc connection.
            If connection type = 'pandas' SQL Alchemy engine

        """

        if default == "SourceData":
            server = "DWSQLSRV"
            database = "SourceData"
            server_type = "SQL Server"

        elif default == "AutomatedTasks":
            server = "DWSQLSRV"
            database = "AutomatedTasks"
            server_type = "SQL Server"

        elif default == "DW":
            server = "DWSQLSRV"
            database = "DW"
            server_type = "SQL Server"

        elif default == "DW_Metadata":
            server = "DWSQLSRV"
            database = "DW_Metadata"
            server_type = "SQL Server"

        elif default == "DW_Stage":
            server = "DWSQLSRV"
            database = "DW_Stage"
            server_type = "SQL Server"

        elif default == "ML":
            server = "DWSQLSRV"
            database = "ML"
            server_type = "SQL Server"

        elif default == "Vista":
            server = "ERPSQLSRV"
            database = "Viewpoint"
            server_type = "SQL Server"

        elif default == "VP Attachments":
            server = "ERPSQLSRV"
            database = "VPAttachments"
            server_type = "SQL Server"

        elif default == "Tableau":
            server = "tableau.lakesideindustries.com"
            port = 8060
            database = "workgroup"
            server_type = "PostgreSQL"
        elif default:
            raise Exception("Invalid default type")

        return pypyodbc.connect(
            self.__create_connection_string(
                server_type, server, database, username, password, port
            )
        )

    def viewpoint(self):
        """Returns connection to ERPSQLSRV.Viewpoint"""
        return self.create_sql_connection("Vista")

    def vp_attachments(self):
        """Returns connection to ERPSQLSRV.VPAttachments"""
        return self.create_sql_connection("VP Attachments")

    def source_data(self):
        """Returns connection to DWSQLSRV.SourceData"""
        return self.create_sql_connection("SourceData")

    def automated_tasks(self):
        """Returns connection to DWSQLSRV.AutomatedTasks"""
        return self.create_sql_connection("AutomatedTasks")

    def dw(self):
        """Returns connection to DWSQLSRV.DW"""
        return self.create_sql_connection("DW")

    def dw_metadata(self):
        """Returns connection to DWSQLSRV.DW_Metadata"""
        return self.create_sql_connection("DW_Metadata")

    def dw_stage(self):
        """Returns connection to DWSQLSRV.DW_Stage"""
        return self.create_sql_connection("DW_Stage")

    def ml(self):
        """Returns connection to DWSQLSRV.ML"""
        return self.create_sql_connection("ML")

    def tableau(self):
        """Returns connection to the Tableau PosgreSQL Server"""
        return self.create_sql_connection("Tableau")

    def create_pandas_connection(self, server:str, database:str, username:str=None, password:str=None):
        params = urllib.parse.quote_plus(
            f"Driver={{SQL Server}};Server={server};Database={database};Trusted_Connection=Yes;APP={self.app}"
        )

        return params

    def df_to_sql(self, engine, schema, df, name, data_type=None, append=False):


        engine = create_engine(f"mssql+pyodbc:///?odbc_connect={engine}")

        @event.listens_for(engine, "before_cursor_execute")
        def receive_before_cursor_execute(
            conn, cursor, statement, params, context, executemany
        ):
            if executemany:
                cursor.fast_executemany = True
                cursor.commit()

        with engine.connect() as conn:

            if df.shape[0] == 0:
                return

            if data_type is None:
                data_type = {col_name: types.VARCHAR(length=8000) for col_name in df}

            chunk_size = (2100 // len(data_type)) - 1

            df.to_sql(
                name,
                engine,
                schema=schema,
                if_exists= "append" if append else "replace",
                #chunksize=chunk_size,
                #method='multi',
                index=False,
                dtype=data_type,
            )

        engine.dispose()

    def execute_command(self, connection, command, values=None):
        con = connection
        if con is None:
            raise "Connection is None"
        cursor = con.cursor()
        try:

            if values is None:
                cursor.execute(command)
            else:
                cursor.execute(command, values)
            con.commit()
        except Exception as e:
            raise e
        finally:
            con.close()

    def get_geotab_creds(self):
        con = self.source_data()
        cursor = con.cursor()

        command = 'Select Username, Password from Geotab.Credentials'
        
        cursor.execute(command)

        data = cursor.fetchone()

        con.close()

        return (data[0], data[1])

    def get_jjkeller_creds(self):
        con = self.source_data()
        cursor = con.cursor()

        command = 'Select Username, Password from JJKeller.Credentials'
        
        cursor.execute(command)

        data = cursor.fetchone()

        con.close()

        return (data[0], data[1])
