def create_log(connection, id=None, status=None, message=None, task="No Task"):
    """Initialize Log or complete log

    Keyword arguments:

    connection -- A connection from Connection.sql_server

    id -- If completing a log use the Log ID returned previously

    status -- If completing a log use either "success" or "failure"

    message -- If logging "failure" provide optional message

    task -- When creating new log define task
    """
    cursor = connection.cursor()

    if id == None:
        SqlCommand = (
            "Insert into dbo.Logs(Task) " + "Output Inserted.LogID " + "Values(?)"
        )
        Values = [task]
        cursor.execute(SqlCommand, Values)
        id = int(cursor.fetchone()[0])
        connection.commit()
        connection.close()
        return id
    elif status.lower() == "success":
        SqlCommand = (
            "Update dbo.Logs "
            + "Set EndTime = GETDATE(), IsSuccessful = 1 , Message = ? "
            + "Where LogID = ?"
        )
        Values = [message, id]
        cursor.execute(SqlCommand, Values)
        cursor.commit()
        cursor.close()
    else:
        SqlCommand = (
            "Update dbo.Logs "
            + "Set EndTime = GETDATE(), Message = ? "
            + "Where LogID = ?"
        )
        Values = [message, id]
        cursor.execute(SqlCommand, Values)
        cursor.commit()
        cursor.close()


def log_status_update(connection, log_id: int, status: str):

    """Adds log status updates to display progress of long running tasks

    Keyword arguments:

    connection -- A connection from Connection.sql_server

    log_id -- The ID of a log already started

    status -- status message"""

    cursor = connection.cursor()

    command = """INSERT INTO dbo.LogStatus (LogID, StatusText)
                 VALUES (?, ?)"""

    values = [log_id, status]

    cursor.execute(command, values)

    cursor.commit()

    cursor.close()


class PrintLog():
    def __init__(self, connection, log_id) -> None:
        self.log = log_id
        self.connection = connection

    def print_log(self, status: str):
        """Prints log message and records it in Log Status"""
        log_status_update(self.connection, self.log, status)
        print(status)
