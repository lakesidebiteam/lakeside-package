import json
from ldap3 import Server, Connection, ALL, NTLM, SUBTREE
import pandas as pd
from pypyodbc import SQL_VARBINARY
from sqlalchemy import types
import string
import traceback
from ldap3.extend.microsoft.addMembersToGroups import ad_add_members_to_groups
from ldap3.extend.microsoft.removeMembersFromGroups import ad_remove_members_from_groups


from lakeside.tools.phone import clean_phone
from lakeside.log import create_log, log_status_update, PrintLog
from lakeside.connection.sql_server import LakesideConnections
from lakeside.tools.dates import convert_str_utc_pt_str
from lakeside.tools.lists import first_item


class ActiveDirectory:
    def __init__(self, user=None, password=None) -> None:
        self.lc = LakesideConnections()

        self.__user = user
        self.__pass = password

    def __get_ad_creds(self):
        con = self.lc.source_data()
        cursor = con.cursor()

        command = "Select UserName, Password FROM ActiveDirectory.Credentials"

        cursor.execute(command)

        data = cursor.fetchone()

        return (data[0], data[1])

    def create_ad_connection(self, server="ADSRV01.lakesideind.net"):

        if self.__user is None:
            self.__user, self.__pass = self.__get_ad_creds()

        svr = Server(server, get_info=ALL, use_ssl=True)

        con = Connection(svr, user=self.__user, password=self.__pass, authentication=NTLM, auto_bind=True)

        return con

    def reload_person(self, log_id: int = None):
        """Reloads SourceData.ActiveDirectory.Person.

        Keyword Arguments:
        log_id -- Optional LogID if a log has already been started (default None)
        """
        if log_id is None:
            log_id = create_log(self.lc.source_data(), task="Active Directory Person")

        pl = PrintLog(self.lc.source_data(), log_id)

        pl.print_log("")
        pl.print_log(f"Starting Active Directory Person reload with LogID: {log_id}")
        pl.print_log("")

        try:
            conn = self.create_ad_connection()

            attributes = [
                "accountExpires",
                "accountNameHistory",
                "aCSPolicyName",
                "adminCount",
                "adminDescription",
                "adminDisplayName",
                "altSecurityIdentities",
                "assistant",
                "badPasswordTime",
                "badPwdCount",
                "bridgeheadServerListBL",
                "c",
                "canonicalName",
                "cn",
                "co",
                "codePage",
                "comment",
                "company",
                "controlAccessRights",
                "countryCode",
                "createTimeStamp",
                "dBCSPwd",
                "defaultClassStore",
                "department",
                "description",
                "desktopProfile",
                "destinationIndicator",
                "displayName",
                "displayNamePrintable",
                "distinguishedName",
                "division",
                "dSASignature",
                "dynamicLDAPServer",
                "employeeID",
                "extensionName",
                "facsimileTelephoneNumber",
                "flags",
                "fromEntry",
                "frsComputerReferenceBL",
                "fRSMemberReferenceBL",
                "fSMORoleOwner",
                "garbageCollPeriod",
                "generationQualifier",
                "givenName",
                "groupMembershipSAM",
                "groupPriority",
                "groupsToIgnore",
                "homeDirectory",
                "homeDrive",
                "homePhone",
                "homePostalAddress",
                "info",
                "initials",
                "instanceType",
                "internationalISDNNumber",
                "ipPhone",
                "isCriticalSystemObject",
                "isDeleted",
                "isPrivilegeHolder",
                "l",
                "lastKnownParent",
                "lastLogoff",
                "lastLogon",
                "legacyExchangeDN",
                "lmPwdHistory",
                "localeID",
                "lockoutTime",
                "logonCount",
                "logonWorkstation",
                "mail",
                "manager",
                "masteredBy",
                "maxStorage",
                "mhsORAddress",
                "middleName",
                "mobile",
                "modifyTimeStamp",
                "mS-DS-ConsistencyChildCount",
                "mS-DS-CreatorSID",
                "mSMQDigestsMig",
                "mSMQSignCertificatesMig",
                "msNPAllowDialin",
                "msNPCallingStationID",
                "msNPSavedCallingStationID",
                "msRADIUSCallbackNumber",
                "msRADIUSFramedIPAddress",
                "msRADIUSFramedRoute",
                "msRADIUSServiceType",
                "msRASSavedCallbackNumber",
                "msRASSavedFramedIPAddress",
                "msRASSavedFramedRoute",
                "msDS-PrincipalName",
                "name",
                "netbootSCPBL",
                "networkAddress",
                "nonSecurityMemberBL",
                "ntPwdHistory",
                "o",
                "objectCategory",
                "objectGUID",
                "objectSid",
                "objectVersion",
                "operatorCount",
                "otherFacsimileTelephoneNumber",
                "otherHomePhone",
                "otherIpPhone",
                "otherLoginWorkstations",
                "otherMailbox",
                "otherMobile",
                "otherPager",
                "otherTelephone",
                "otherWellKnownObjects",
                "ou",
                "pager",
                "partialAttributeDeletionList",
                "partialAttributeSet",
                "personalTitle",
                "physicalDeliveryOfficeName",
                "possibleInferiors",
                "postalAddress",
                "postalCode",
                "postOfficeBox",
                "preferredDeliveryMethod",
                "preferredOU",
                "primaryGroupID",
                "primaryInternationalISDNNumber",
                "primaryTelexNumber",
                "profilePath",
                "proxiedObjectName",
                "pwdLastSet",
                "queryPolicyBL",
                "registeredAddress",
                "replUpToDateVector",
                "repsFrom",
                "repsTo",
                "revision",
                "rid",
                "sAMAccountName",
                "sAMAccountType",
                "scriptPath",
                "sDRightsEffective",
                "securityIdentifier",
                "seeAlso",
                "serverReferenceBL",
                "showInAdvancedViewOnly",
                "sIDHistory",
                "siteObjectBL",
                "sn",
                "st",
                "street",
                "streetAddress",
                "subRefs",
                "subSchemaSubEntry",
                "supplementalCredentials",
                "systemFlags",
                "telephoneNumber",
                "teletexTerminalIdentifier",
                "telexNumber",
                "terminalServer",
                "textEncodedORAddress",
                "thumbnailLogo",
                "title",
                "unicodePwd",
                "url",
                "userAccountControl",
                "userCert",
                "userPassword",
                "userPrincipalName",
                "userSharedFolder",
                "userSharedFolderOther",
                "userSMIMECertificate",
                "userWorkstations",
                "uSNChanged",
                "uSNCreated",
                "uSNDSALastObjRemoved",
                "USNIntersite",
                "uSNLastObjRem",
                "uSNSource",
                "wbemPath",
                "wellKnownObjects",
                "whenChanged",
                "whenCreated",
                "wWWHomePage",
                "x121Address",
            ]

            phone_fields = [
                "telephoneNumber",
                "mobile",
                "facsimileTelephoneNumber",
                "homePhone",
                "msRADIUSCallbackNumber",
                "msRASSavedCallbackNumber",
                "otherFacsimileTelephoneNumber",
                "otherHomePhone",
                "otherIpPhone",
                "otherMobile",
                "otherPager",
                "otherTelephone",
            ]

            people = []

            letters = [i for i in string.ascii_uppercase]
            letters.extend(list(range(0, 10)))

            for letter in letters:
                pl.print_log(f"Loading Accounts starting with {letter}")
                conn.search(
                    "dc=lakesideind,dc=net",
                    f"(&(objectclass=person)(samAccountName={letter}*))",
                    attributes=attributes,
                )
                pl.print_log(f"{len(conn.entries)} accounts loaded.")
                pl.print_log("")
                for entry in conn.entries:
                    people.append(json.loads(entry.entry_to_json()))

            data = {attribute: [] for attribute in attributes}
            data["dn"] = []

            for person in people:
                data["dn"] = person["dn"]
                attr = person["attributes"]
                for attribute in attributes:
                    if len(attr[attribute]) == 0:
                        data[attribute].append(None)
                    elif len(attr[attribute]) == 1:
                        data[attribute].extend(attr[attribute])
                    else:
                        data[attribute].append(", ".join(str(attr[attribute])))

            df = pd.DataFrame(data)

            for field in phone_fields:
                df[field] = df[field].apply(clean_phone)

            long_columns = ["description"]

            data_type = {
                col_name: types.VARCHAR(length=500)
                if col_name in long_columns
                else types.VARCHAR(length=255)
                for col_name in df
            }

            pl.print_log(f"{df.shape[0]} records in dataframe")

            pl.print_log("Loading Data to SQL")
            pl.print_log("")

            self.lc.df_to_sql(
                self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
                "ActiveDirectory",
                df,
                "Person",
                data_type,
            )

            pl.print_log("Moving data from stage")
            pl.print_log("")

            self.lc.execute_command(
                self.lc.dw_metadata(),
                "{ CALL pMoveExternalDataFromStagetoFinalStage(?) }",
                [113],
            )

            # Move data from Stage to SourceData

            create_log(self.lc.source_data(), log_id, "success")

            pl.print_log("Active Directory Person reload complete")

        except:
            pl.print_log(traceback.format_exc())
            create_log(self.lc.source_data(), log_id, "failure", traceback.format_exc())

    def reload_groups(self, log_id: int = None):

        if log_id is None:
            log_id = create_log(self.lc.source_data(), task="Active Directory Groups")
        else:
            log_id = log_id

        pl = PrintLog(self.lc.source_data(), log_id)

        pl.print_log("")
        pl.print_log(f"Starting Active Directory Groups reload with LogID: {log_id}")
        pl.print_log("")

        try:

            pl.print_log("Creating AD Connection")
            con = self.create_ad_connection()
            pl.print_log("AD Connection Created")

            attributes = [
                "canonicalName",
                "cn",
                "createTimeStamp",
                "description",
                "displayName",
                "displayNamePrintable",
                "distinguishedName",
                "groupType",
                "info",
                "legacyExchangeDN",
                "mail",
                "managedBy",
                "member",
                "memberOf",
                "modifyTimeStamp",
                "msExchHideFromAddressLists",
                "msExchRequireAuthToSendTo",
                "name",
                "objectCategory",
                "objectClass",
                "objectGUID",
                "objectSid",
                "primaryGroupToken",
                "reportToOriginator",
                "reportToOwner",
                "sAMAccountName",
                "telephoneNumber",
                "textEncodedORAddress",
                "uSNChanged",
                "uSNCreated",
                "whenChanged",
                "whenCreated",
            ]

            pl.print_log("")
            pl.print_log("Getting Groups from AD")
            con.search(
                "dc=lakesideind,dc=net", "(objectclass=group)", attributes=attributes
            )
            pl.print_log("Completed Getting Groups from AD")

            pl.print_log("")
            pl.print_log("Processing data")

            groups = []
            group_members = {"group_dn": [], "member_dn": []}
            group_member_of = {"parent_dn": [], "child_dn": []}

            for entry in con.entries:

                data = json.loads(entry.entry_to_json())
                attr = data.get("attributes", {})
                data["cn"] = " - ".join(attr.pop("cn", []))
                data["canonicalName"] = " - ".join(attr.pop("canonicalName", []))
                data["createTimeStamp"] = convert_str_utc_pt_str(
                    attr.pop("createTimeStamp", [""])[0]
                )
                data["description"] = " ".join(attr.pop("description", []))
                data["displayName"] = " - ".join(attr.pop("displayName", []))
                data["displayNamePrintable"] = " - ".join(
                    attr.pop("displayNamePrintable", [])
                )
                data["distinguishedName"] = " - ".join(
                    attr.pop("distinguishedName", [])
                )
                data["groupType"] = attr.pop("groupType", [None])[0]
                data["info"] = " ".join(attr.pop("info", []))
                data["legacyExchangeDN"] = ";".join(attr.pop("legacyExchangeDN", []))
                data["mail"] = ";".join(attr.pop("mail", []))
                data["managedBy"] = ";".join(attr.pop("managedBy", []))
                data["modifyTimeStamp"] = convert_str_utc_pt_str(
                    first_item(attr.pop("modifyTimeStamp", [""]))
                )
                data["msExchHideFromAddressLists"] = first_item(
                    attr.pop("msExchHideFromAddressLists", [None])
                )
                data["msExchRequireAuthToSendTo"] = first_item(
                    attr.pop("msExchRequireAuthToSendTo", [None])
                )
                data["name"] = first_item(attr.pop("name", [None]))
                data["objectCategory"] = first_item(attr.pop("objectCategory", [None]))
                data["objectClass"] = ";".join(attr.pop("objectClass", []))
                data["objectGUID"] = first_item(attr.pop("objectGUID", [None]))
                data["objectSid"] = first_item(attr.pop("objectSid", [None]))
                data["primaryGroupToken"] = first_item(
                    attr.pop("primaryGroupToken", [None])
                )
                data["reportToOriginator"] = first_item(
                    attr.pop("reportToOriginator", [None])
                )
                data["reportToOwner"] = first_item(attr.pop("reportToOwner", [None]))
                data["sAMAccountName"] = first_item(attr.pop("sAMAccountName", [None]))
                data["telephoneNumber"] = first_item(
                    attr.pop("telephoneNumber", [None])
                )
                data["textEncodedORAddress"] = first_item(
                    attr.pop("textEncodedORAddress", [None])
                )
                data["uSNChanged"] = first_item(attr.pop("uSNChanged", [None]))
                data["uSNCreated"] = first_item(attr.pop("uSNCreated", [None]))
                data["whenChanged"] = convert_str_utc_pt_str(
                    first_item(attr.pop("whenChanged", [""]))
                )
                data["whenCreated"] = convert_str_utc_pt_str(
                    first_item(attr.pop("whenCreated", [""]))
                )
                data.pop("attributes")

                for member in attr.pop("member", []):
                    group_members["group_dn"].append(data["dn"])
                    group_members["member_dn"].append(member)
                for parent in attr.pop("memberOf", []):
                    group_member_of["parent_dn"].append(parent)
                    group_member_of["child_dn"].append(data["dn"])

                groups.append(data)

            group_df = pd.DataFrame(groups)
            members_df = pd.DataFrame(group_members)
            members_of_df = pd.DataFrame(group_member_of)

            pl.print_log("Completed Processing Data")
            pl.print_log("")

            pl.print_log("Transferring Data to SQL")

            self.lc.df_to_sql(
                self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
                "ActiveDirectory",
                group_df,
                "Groups",
            )
            self.lc.df_to_sql(
                self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
                "ActiveDirectory",
                members_df,
                "GroupMembership",
            )
            self.lc.df_to_sql(
                self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
                "ActiveDirectory",
                members_of_df,
                "GroupMemberOf",
            )

            pl.print_log("Transfer Complete")
            pl.print_log("")

            pl.print_log("Moving Data")

            self.lc.execute_command(
                self.lc.source_data(), "{ CALL ActiveDirectory.pMoveGroups }"
            )
            pl.print_log("Moving Data Complete")

            create_log(self.lc.source_data(), id=log_id, status="success")
        except:
            pl.print_log(traceback.format_exc())
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def __get_group_dn(self, group):
        con = self.create_ad_connection()

        con.search(
                search_base="DC=lakesideind,DC=net",
                search_filter=f"(&(objectClass=GROUP)(cn={group}))",
                search_scope=SUBTREE,
                attributes=["distinguishedName"],
            )

        return str(con.entries[0]).splitlines()[1].replace("distinguishedName: ", "").strip()

    def get_group_membership(self, group):
        con = self.create_ad_connection()

        dn = self.__get_group_dn(group)

        con.search(
            search_base="DC=lakesideind,DC=net",
            search_filter=f"(&(objectCategory=user)(memberOf={dn}))",
            search_scope=SUBTREE,
            attributes=["msDS-PrincipalName", "distinguishedName"],
        )

        member_dn = []
        member_login = []

        for member in con.entries:
            member = str(str(member).splitlines()[1:3])
            cn = member.replace("msDS-PrincipalName: ", "").replace(
                "distinguishedName: ", ""
            )

            cn = cn.split(", ")

            for i, val in enumerate(cn):
                val = val.replace("[", "")
                val = val.replace("]", "")
                val = val.replace("'", "")
                val = val.replace("\\\\", "\\")
                val = val.strip()
                cn[i] = val

            member_dn.append(cn[0])
            member_login.append(cn[1])

        members_df = pd.DataFrame({"dn": member_dn, "login": member_login})

        return members_df

    def add_user_to_ad_group(self, account_dn, group):
        group_dn = self.__get_group_dn(group)

        con = self.create_ad_connection()
        
        ad_add_members_to_groups(con, account_dn, group_dn, fix=True, raise_error=True)

    def remove_user_from_ad_group(self, account_dn, group):
        group_dn = self.__get_group_dn(group)

        con = self.create_ad_connection()
        
        ad_remove_members_from_groups(con, account_dn, group_dn, fix=True, raise_error=True)