from cmath import nan
import mygeotab
import pandas as pd
from pandas.core.frame import DataFrame
import pytz
import dateutil
import traceback
import re
import decimal
from sqlalchemy import types
import traceback

from lakeside.connection.sql_server import LakesideConnections
from lakeside.log import create_log
from lakeside.tools.phone import clean_phone
from lakeside.tools.colors import rgba_to_hex
from lakeside.tools.dates import time_to_seconds
from lakeside.tools.units import km_to_mi


class Geotab:
    def __init__(
        self, existing_client=None, username: str = None, password: str = None
    ) -> None:
        self.lc = LakesideConnections()
        self.username = username
        self.password = password
        if existing_client is not None:
            self.client = existing_client
        else:
            self.client = None

    def create_client(self):

        if self.username is None:
            self.username, self.password = self.lc.get_geotab_creds()

        self.client = mygeotab.API(
            username=self.username,
            password=self.password,
            database="lakesideindustries",
        )
        self.client.authenticate()

    def __convert_to_int(self, num):
        return int(num, 16)

    def __float_to_str(self, f):
        """
        Convert the given float to a string,
        without resorting to scientific notation
        """
        if f is None:
            return None
        ctx = decimal.Context()
        ctx.prec = 20

        d1 = ctx.create_decimal(repr(f))
        return format(d1, "f")

    def __exec_command(self, command: str, values: list = None):
        self.lc.execute_command(self.lc.source_data(), command=command, values=values)

    def __parse_id_to_text(self, value, id_type):
        if type(value) != str:
            return None
        value = value.replace(id_type, "")

        if value.endswith("Id"):
            value = value[0 : len(value) - 2]

        value = self.__add_spacing(value)

        value = value.replace(" Per ", " per ")

        if value == "None":
            return None

        return value

    def __add_spacing(self, text):
        if type(text) != str:
            return None

        return re.sub(r"(?<=\w)([A-Z])", r" \1", text)

    def convert_utc_pt_str(self, datetime, time_zone=None) -> str:
        """Converts utc time to local time and returns it as a string

        Keyword Arguments:
        datetime - datetime object to be converted
        time_zone - timezone for date to be converted to (Default None is US/Pacific)
        """

        if pd.isnull(datetime):
            return None

        if type(datetime) == str:
            datetime = pytz.utc.localize(dateutil.parser.parse(datetime))

        if time_zone is None:
            time_zone = dateutil.tz.gettz("US/Pacific")

        conv_dt = datetime.astimezone(time_zone)
        return str(conv_dt)[:19]

    def __df_to_sql(self, df: DataFrame, name: str, data_types=None):
        engine = self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage")
        self.lc.df_to_sql(
            engine,
            "Geotab",
            df,
            name,
            data_types,
        )

    def duration_to_seconds(self, duration):
        seconds = 0
        if duration is None:
            return None
        elif type(duration) == str:
            if len(duration.split(".")) == 3:
                day, time, frac = duration.split(".")
            else:
                day, time = duration.split(".")
                frac = "0"

            seconds += int(day) * 86400  # seconds per day

            h, m, s = time.split(":")

            seconds += int(s)
            seconds += int(m) * 60
            seconds += int(h) * 3600  # seconds in hour
            seconds += 1 if int(frac[0]) >= 5 else 0
        elif type(duration) == int:
            return duration

        else:
            seconds = time_to_seconds(duration)  # seconds in hour

        return seconds

    def get_current_device_status(self, device_ids: list = None):
        """Returns the current device status and location

        Keyword Arguments:
        device_ids - a list of device ids (Default None to return all)
        """

        if self.client is None:
            self.create_client()

        if device_ids is None:
            device_status = self.client.get("DeviceStatusInfo")
        else:
            device_status = []
            for id in device_ids:
                device_status.append(self.client.get("DeviceStatusInfo", id=id))

        device_dict = {}
        for i in device_status:
            device_dict[i["device"]["id"]] = i

        return device_dict

    def update_dw_equipment_locations(self, status_list: list = None):

        if status_list is None:
            status_dict = self.get_current_device_status()
            status_list = [status_dict[i] for i in status_dict.keys()]

        con = self.lc.dw()
        cursor = con.cursor()

        command = "{ CALL EQ.pUpdateEquipmentLocation(?, ?, ?, ?, ?, ?, ?, ?, ?) }"

        for status in status_list:

            if type(status.get("driver")) == dict:
                driver = status["driver"].get("id")
            else:
                driver = None

            values = [
                status["device"]["id"],
                status.get("latitude"),
                status.get("longitude"),
                self.convert_utc_pt_str(status.get("dateTime")),
                status.get("speed"),
                status.get("bearing"),
                driver,
                status.get("isDeviceCommunicating", False),
                status.get("isDriving", False),
            ]

            cursor.execute(command, values)

        cursor.commit()
        con.close()

    def get_devices(self, device_ids: list = None):
        """Returns a dictionary of devices and details

        Keyword Arguments:
        device_ids - a list of device ids (Default None to return all)
        """
        if self.client is None:
            self.create_client()

        if device_ids is None:
            device = self.client.get("Device")
        else:
            device = []
            for id in device_ids:
                device.append(self.client.get("Device", id=id))

        device_dict = {}
        for i in device:
            device_dict[i["id"]] = i

        return device_dict

    def device_status_to_sql(self):
        """Loads device statuses to SourceData_Stage"""

        log_id = create_log(self.lc.source_data(), task="Geotab Device Status")

        try:
            statuses = self.get_current_device_status()
            status_list = [statuses[i] for i in statuses.keys()]
            status_df = pd.DataFrame(status_list)

            date_cols = ["dateTime"]

            for col in date_cols:
                status_df[col] = status_df[col].apply(self.convert_utc_pt_str)

            status_df["device"] = status_df["device"].apply(pd.Series)

            include_cols = ["device", "isDeviceCommunicating"]

            self.__df_to_sql(status_df[include_cols], "DeviceStatus")

            create_log(self.lc.source_data(), log_id, "Success")

        except Exception as e:
            traceback.print_exc()

            create_log(self.lc.source_data(), log_id, "Failure", traceback.format_exc())

    def devices_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="Geotab Devices")

        try:

            devices = self.get_devices()
            device_list = [devices[i] for i in devices.keys()]
            device_df = pd.DataFrame(device_list)

            date_cols = ["activeFrom", "activeTo", "ignoreDownloadsUntil"]

            for col in date_cols:
                device_df[col] = device_df[col].apply(self.convert_utc_pt_str)

            include_cols = [
                "obdAlertEnabled",
                "enableControlExternalRelay",
                "externalDeviceShutDownDelay",
                "immobilizeArming",
                "immobilizeUnit",
                "accelerationWarningThreshold",
                "accelerometerThresholdWarningFactor",
                "brakingWarningThreshold",
                "corneringWarningThreshold",
                "enableBeepOnDangerousDriving",
                "enableBeepOnRpm",
                "engineHourOffset",
                "isActiveTrackingEnabled",
                "isDriverSeatbeltWarningOn",
                "isPassengerSeatbeltWarningOn",
                "isReverseDetectOn",
                "isIoxConnectionEnabled",
                "odometerFactor",
                "odometerOffset",
                "rpmValue",
                "seatbeltWarningSpeed",
                "activeFrom",
                "activeTo",
                "disableBuzzer",
                "enableBeepOnIdle",
                "enableMustReprogram",
                "enableSpeedWarning",
                "engineType",
                "engineVehicleIdentificationNumber",
                "ensureHotStart",
                "gpsOffDelay",
                "idleMinutes",
                "isSpeedIndicator",
                "licensePlate",
                "licenseState",
                "major",
                "minAccidentSpeed",
                "minor",
                "parameterVersion",
                "pinDevice",
                "speedingOff",
                "speedingOn",
                "vehicleIdentificationNumber",
                "goTalkLanguage",
                "fuelTankCapacity",
                "disableSleeperBerth",
                "autoHos",
                "parameterVersionOnDevice",
                "comment",
                "timeZoneId",
                "deviceType",
                "id",
                "ignoreDownloadsUntil",
                "maxSecondsBetweenLogs",
                "name",
                "productId",
                "serialNumber",
                "timeToDownload",
                "workTime",
            ]

            self.__df_to_sql(device_df[include_cols], "Device")

            self.device_status_to_sql()

            self.__exec_command("{ CALL Geotab.pMoveDevice }")

            create_log(self.lc.source_data(), log_id, "Success")

        except Exception as e:
            traceback.print_exc()

            create_log(self.lc.source_data(), log_id, "Failure", traceback.format_exc())

    def get_users(self, user_ids: list = None, drivers_only: bool = False):
        """Returns a dictionary of users and details

        Keyword Arguments:
        user_ids - a list of user ids (Default None to return all)
        drivers_only - only return drivers (Default False)
        """
        if self.client is None:
            self.create_client()

        if user_ids is None:
            if drivers_only:
                user = self.client.get("User", isDriver=True)
            else:
                user = self.client.get("User")
        else:
            user = []
            for id in user_ids:
                if drivers_only:
                    user.append(self.client.get("User", id=id, isDriver=True))
                else:
                    user.append(self.client.get("User", id=id))

        user_dict = {}
        for i in user:
            user_dict[i["id"]] = i

        return user_dict

    def users_to_sql(self):
        log_id = create_log(self.lc.source_data(), task="Geotab Users")

        try:

            users = self.get_users()
            user_list = [users[i] for i in users.keys()]
            user_df = pd.DataFrame(user_list)

            date_cols = ["activeFrom", "activeTo", "lastAccessDate"]

            for col in date_cols:
                user_df[col] = user_df[col].apply(self.convert_utc_pt_str)

            user_df["phoneNumber"] = user_df["phoneNumber"].apply(clean_phone)

            groups = {"user": [], "group": []}

            for key, value in users.items():
                for group in value["companyGroups"]:
                    groups["user"].append(key)
                    groups["group"].append(group["id"])

            group_df = pd.DataFrame(groups)

            include_cols = [
                "viewDriversOwnDataOnly",
                "licenseProvince",
                "licenseNumber",
                "acceptedEULA",
                "driveGuideVersion",
                "wifiEULA",
                "activeFrom",
                "activeTo",
                "changePassword",
                "comment",
                "dateFormat",
                "phoneNumber",
                "countryCode",
                "phoneNumberExtension",
                "defaultGoogleMapStyle",
                "defaultMapEngine",
                "defaultOpenStreetMapStyle",
                "defaultHereMapStyle",
                "defaultPage",
                "designation",
                "employeeNo",
                "firstName",
                "fuelEconomyUnit",
                "electricEnergyEconomyUnit",
                "hosRuleSet",
                "isYardMoveEnabled",
                "isPersonalConveyanceEnabled",
                "isExemptHOSEnabled",
                "authorityName",
                "authorityAddress",
                "id",
                "isEULAAccepted",
                "isNewsEnabled",
                "isServiceUpdatesEnabled",
                "isLabsEnabled",
                "isMetric",
                "language",
                "firstDayOfWeek",
                "lastName",
                "name",
                "showClickOnceWarning",
                "timeZoneId",
                "userAuthenticationType",
                "zoneDisplayMode",
                "companyName",
                "companyAddress",
                "carrierNumber",
                "lastAccessDate",
                "isDriver",
                "isEmailReportEnabled",
            ]

            self.__df_to_sql(user_df[include_cols], "User")
            self.__df_to_sql(group_df, "UserGroups")

            self.__exec_command("{ CALL Geotab.pMoveUser }")

            create_log(self.lc.source_data(), log_id, "Success")

        except Exception as e:
            traceback.print_exc()

            create_log(self.lc.source_data(), log_id, "Failure", traceback.format_exc())

    def get_feed_version(self, feed: str):
        """Gets the latest version from a feed

        Keyword Arguements
        feed - the feed name e.g. DeviceStatusInfo
        """
        con = self.lc.source_data()
        cursor = con.cursor()

        command = "Select Version from Geotab.FeedVersion Where Feed = ?"
        values = [feed]

        cursor.execute(command, values)

        data = cursor.fetchone()

        cursor.commit()
        cursor.close()

        if data is None:
            return None
        else:
            return data[0]

    def set_feed_version(self, feed: str, version: str):

        """Sets the latest verion of a feed

        Keyword Arguements
        feed - the feed name e.g. DeviceStatusInfo
        version - the latest version
        """

        con = self.lc.source_data()
        cursor = con.cursor()

        command = """
            DECLARE @Feed varchar(255) = ?
                    ,@version varchar(25) = ?
            
            IF @Feed in(SELECT Feed from Geotab.FeedVersion)
                UPDATE Geotab.FeedVersion
                SET Version = @Version
                WHERE Feed = @Feed
            ELSE
                INSERT INTO Geotab.FeedVersion (Feed, Version)
                VALUES (@Feed, @Version)
            """
        values = [feed, version]

        cursor.execute(command, values)
        cursor.commit()
        cursor.close()

    def dutystatuslog_to_sql(self, logs: list):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return None

        for i in range(len(logs)):
            logs[i]["odometer"] = str(logs[i].get("odometer"))
            logs[i]["distanceSinceValidCoordinates"] = str(
                logs[i].get("distanceSinceValidCoordinates")
            )

        ds_df = pd.DataFrame(logs)
        pd.options.display.float_format = "{:,.0f}".format

        if "location" in ds_df.columns:
            ds_df = pd.concat(
                [
                    ds_df,
                    ds_df["location"]
                    .apply(pd.Series)["location"]
                    .apply(pd.Series)[["x", "y"]],
                ],
                1,
            )
        else:
            ds_df["x"] = None
            ds_df["y"] = None

        ds_df["device"] = ds_df["device"].apply(get_id)
        ds_df["driver"] = ds_df["driver"].apply(get_id)

        if "editRequestedByUser" in ds_df.columns:
            ds_df["editRequestedByUser"] = ds_df["editRequestedByUser"].apply(
                pd.Series
            )["id"]
        else:
            ds_df["editRequestedByUser"] = None

        ds_df["sequence"] = ds_df["sequence"].apply(self.__convert_to_int)
        ds_df["version"] = ds_df["version"].apply(self.__convert_to_int)
        date_cols = ["editDateTime", "dateTime", "verifyDateTime"]

        for col in date_cols:
            if col not in ds_df.columns:
                ds_df[col] = None
            else:
                ds_df[col] = pd.to_datetime(ds_df[col]).apply(self.convert_utc_pt_str)

        include_cols = [
            "editDateTime",
            "id",
            "status",
            "origin",
            "device",
            "dateTime",
            "driver",
            "state",
            "malfunction",
            "version",
            "sequence",
            "eventRecordStatus",
            "deferralStatus",
            "deferralMinutes",
            "isIgnored",
            "eventCheckSum",
            "eventCode",
            "eventType",
            "odometer",
            "verifyDateTime",
            "distanceSinceValidCoordinates",
            "engineHours",
            "editRequestedByUser",
            "x",
            "y",
        ]

        for col in include_cols:
            if col not in ds_df.columns:
                ds_df[col] = None

        self.__df_to_sql(ds_df[include_cols], "DutyStatusLog")

        self.__exec_command("{ CALL Geotab.pMoveDutyStatusLog }")

    def duty_status_availability_to_sql(self):
        """ Loads Duty Status Availability to SourceData """

        log_id = create_log(
            self.lc.source_data(), task="Geotab Duty Status Availability"
        )

        try:
            if self.client is None:
                self.create_client()

            ds = self.client.get("DriverRegulation")

            availability = []
            cycle_summary = []
            day_summary = []
            recap = []
            workday_summary = []
            cycle_availabilities = []

            for stat in ds:
                if type(stat["availability"]["driver"]) == dict:
                    driver = stat["availability"]["driver"]["id"]
                else:
                    driver = stat["availability"]["driver"]

                stat["availability"]["driver"] = driver
                stat["availability"]["offDutyNeeded"] = stat.get("offDutyNeeded")
                stat["availability"]["restBreakNeeded"] = stat.get("restBreakNeeded")

                durations = [
                    "cycle",
                    "rest",
                    "driving",
                    "workday",
                    "offDutyNeeded",
                    "restBreakNeeded",
                    "duty",
                ]

                for dur in durations:
                    stat["availability"][dur] = self.duration_to_seconds(
                        stat["availability"].get(dur)
                    )

                availability.append(stat["availability"])

                for cs in stat.get("cycleSummaries", []):
                    cs["driver"] = driver
                    cs["consecutiveRest"] = self.duration_to_seconds(
                        cs.get("consecutiveRest")
                    )
                    cs["dateTime"] = self.convert_utc_pt_str(cs.get("dateTime"))
                    cycle_summary.append(cs)

                for day_sum in stat.get("daySummaries", []):
                    day_sum["driver"] = driver
                    day_sum["offTotal"] = self.duration_to_seconds(
                        day_sum.get("offTotal")
                    )
                    day_sum["onTotal"] = self.duration_to_seconds(
                        day_sum.get("onTotal")
                    )
                    day_sum["driveTotal"] = self.duration_to_seconds(
                        day_sum.get("driveTotal")
                    )
                    day_sum["dateTime"] = str(day_sum["dateTime"])[:10]
                    day_summary.append(day_sum)

                for ws in stat.get("workdaySummaries", []):
                    ws["driver"] = driver
                    ws["fromDate"] = self.convert_utc_pt_str(ws.get("fromDate"))
                    ws["toDate"] = self.convert_utc_pt_str(ws.get("toDate"))
                    workday_summary.append(ws)

                for ca in stat["availability"].get("cycleAvailabilities", []):
                    ca["available"] = self.duration_to_seconds(ca.get("available"))
                    ca["driver"] = driver
                    ca["dateTime"] = self.convert_utc_pt_str(ca.get("dateTime"))

                    cycle_availabilities.append(ca)

                for rec in stat["availability"].get("recap"):
                    rec["driver"] = driver
                    rec["dateTime"] = self.convert_utc_pt_str(rec.get("dateTime"))
                    rec["duration"] = self.duration_to_seconds(rec.get("duration"))
                    recap.append(rec)

            # Availability to SQL
            av_df = pd.DataFrame(availability)

            include_cols = [
                "driver",
                "driving",
                "cycle",
                "workday",
                "is16HourExemptionAvailable",
                "isAdverseDrivingExemptionAvailable",
                "offDutyNeeded",
                "restBreakNeeded",
                "rest",
                "duty",
            ]

            self.__df_to_sql(av_df[include_cols], "DutyStatusAvailabilities")

            # Cycle Summary to SQL
            cs_df = pd.DataFrame(cycle_summary)
            self.__df_to_sql(cs_df, "DutyStatusCycleSummary")

            # Day Summary to SQL
            ds_df = pd.DataFrame(day_summary)
            self.__df_to_sql(ds_df, "DutyStatusDaySummary")

            # Workday Summary to SQL
            ws_df = pd.DataFrame(workday_summary)
            self.__df_to_sql(ws_df, "DutyStatusWorkdaySummary")

            # Cycle Availability to SQL
            ca_df = pd.DataFrame(cycle_availabilities)
            self.__df_to_sql(ca_df, "DutyStatusCycleAvailability")

            # Recap to SQL
            re_df = pd.DataFrame(recap)
            self.__df_to_sql(re_df, "DutyStatusRecap")

            self.__exec_command("{ CALL Geotab.pMoveDutyStatusAvailability }")

            create_log(self.lc.source_data(), id=log_id, status="success")

        except Exception as e:
            traceback.print_exc()
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def groups_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="Geotab Groups")

        try:
            if self.client is None:
                self.create_client()

            groups = self.client.get("Group")

            children = {
                "parent": [],
                "child": [],
            }

            for group in groups:
                group["color"] = "#{:02x}{:02x}{:02x}{:02x}".format(
                    group["color"]["r"],
                    group["color"]["g"],
                    group["color"]["b"],
                    group["color"]["a"],
                )
                for child in group["children"]:
                    children["parent"].append(group["id"])
                    children["child"].append(child["id"])

            group_df = pd.DataFrame(groups)
            children_df = pd.DataFrame(children)

            include_cols = ["color", "comments", "id", "name"]

            self.__df_to_sql(group_df[include_cols], "Groups")
            self.__df_to_sql(children_df, "GroupChildren")

            self.__exec_command("{ CALL Geotab.pMoveGroups }")

            create_log(self.lc.source_data(), id=log_id, status="success")

        except Exception as e:
            traceback.print_exc()
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def logs_to_sql(self, logs: list):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return None

        cols = ["latitude", "longitude", "speed", "dateTime", "device", "id"]

        log_df = pd.DataFrame(logs)

        for col in cols:
            if col not in log_df.columns:
                log_df[col] = None

        log_df["device"] = log_df["device"].apply(get_id)

        log_df["dateTime"] = pd.to_datetime(log_df["dateTime"]).apply(
            self.convert_utc_pt_str
        )

        self.__df_to_sql(log_df, "DeviceLog")

        self.__exec_command("{ CALL Geotab.pMoveDeviceLogs }")

    def diagnostic_to_sql(self, diags: list = None):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return None

        if diags is None:
            diags = self.client.get("Diagnostic")

        for i in range(len(diags)):
            diags[i]["conversion"] = self.__float_to_str(diags[i].get("conversion"))
            diags[i]["dataLength"] = str(diags[i].get("dataLength"))
            diags[i]["offset"] = str(diags[i].get("offset"))

        cols = [
            "engineType",
            "code",
            "controller",
            "diagnosticType",
            "faultResetMode",
            "id",
            "name",
            "source",
            "unitOfMeasure",
            "validLoggingPeriod",
            "isLogGuaranteedOnEstimateError",
            "version",
            "parameterGroup",
            "conversion",
            "dataLength",
            "offset",
        ]

        diag_df = pd.DataFrame(diags)

        for col in cols:
            if col not in diag_df.columns:
                diag_df[col] = None

        diag_df["controller"] = diag_df["controller"].apply(get_id)

        diag_df["unitOfMeasure"] = diag_df["unitOfMeasure"].apply(
            self.__parse_id_to_text, id_type="UnitOfMeasure"
        )

        diag_df["engineType"] = diag_df["engineType"].apply(
            self.__parse_id_to_text, id_type="EngineType"
        )

        diag_df["faultResetMode"] = diag_df["faultResetMode"].apply(self.__add_spacing)

        diag_df["source"] = diag_df["source"].apply(
            self.__parse_id_to_text, id_type="Source"
        )

        diag_df["parameterGroup"] = diag_df["parameterGroup"].apply(
            self.__parse_id_to_text, id_type="ParameterGroup"
        )

        self.__df_to_sql(diag_df, "Diagnostic")

        self.__exec_command("{ CALL Geotab.pMoveDiagnostic }")

    def controller_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="Geotab Controllers")

        try:

            if self.client is None:
                self.create_client()

            ctlr = self.client.get("Controller")

            cols = [
                "version",
                "codeId",
                "id",
                "name",
                "source",
                "code",
            ]

            ctlr_df = pd.DataFrame(ctlr)

            for col in cols:
                if col not in ctlr_df.columns:
                    ctlr_df[col] = None

            ctlr_df["source"] = ctlr_df["source"].apply(
                self.__parse_id_to_text, id_type="Source"
            )

            self.__df_to_sql(ctlr_df, "Controller")

            self.__exec_command("{ CALL Geotab.pMoveController }")

            create_log(self.lc.source_data(), id=log_id, status="success")
        except Exception as e:
            print(traceback.print_exc())
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def trailer_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="Geotab Trailers")

        try:

            if self.client is None:
                print("Creating Client\n")
                self.create_client()

            print("Getting data\n")
            trailer = self.client.get("Trailer")

            include_cols = ["comment", "id", "name", "version", "groups"]

            for i, row in enumerate(trailer):
                trailer[i]["groups"] = row.get("groups", [{}])[0].get("id")

            trailer_df = pd.DataFrame(trailer)
            for col in include_cols:
                if col not in trailer_df.columns:
                    trailer_df[col] = None

            print("Transferring to SQL\n")
            self.__df_to_sql(trailer_df, "Trailer")
            print("Moving data\n")
            self.__exec_command("{ CALL Geotab.pMoveTrailer }")

            create_log(self.lc.source_data(), id=log_id, status="success")
            print("Process complete")
        except Exception as e:
            print(traceback.print_exc())
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def zone_type_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="Geotab Zone Types")

        try:

            if self.client is None:
                print("Creating Client\n")
                self.create_client()

            print("Getting data\n")
            zones = self.client.get("ZoneType")

            zone_dict = {
                "id": [],
                "name": [],
                "comment": [],
                "is_custom": [],
            }

            for zone in zones:
                if type(zone) == str:
                    zone_dict["id"].append(zone)
                    zone_dict["name"].append(self.__parse_id_to_text(zone, "ZoneType"))
                    zone_dict["comment"].append(None)
                    zone_dict["is_custom"].append(False)
                else:
                    zone_dict["id"].append(zone["id"])
                    zone_dict["name"].append(zone.get("name"))
                    zone_dict["comment"].append(zone.get("commment"))
                    zone_dict["is_custom"].append(True)

            zone_df = pd.DataFrame(zone_dict)

            print("Transferring to SQL\n")
            self.__df_to_sql(zone_df, "ZoneType")
            print("Moving data\n")
            self.__exec_command("{ CALL Geotab.pMoveZoneType }")

            create_log(self.lc.source_data(), id=log_id, status="success")
            print("Process complete")
        except Exception as e:
            print(traceback.print_exc())
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def zones_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="Geotab Zones")

        try:

            if self.client is None:
                print("Creating Client\n")
                self.create_client()

            print("Getting data\n")
            zone = self.client.get("Zone")

            zone_types = {
                "zone_id": [],
                "type_id": [],
            }

            zone_points = {
                "zone_id": [],
                "latitude": [],
                "longitude": [],
                "sequence": [],
            }

            for z in zone:
                z["activeFrom"] = self.convert_utc_pt_str(z.get("activeFrom"))
                z["activeTo"] = self.convert_utc_pt_str(z.get("activeTo"))
                z["fillColor"] = rgba_to_hex(
                    z.get("fillColor", {}).get("r", 255),
                    z.get("fillColor", {}).get("g", 255),
                    z.get("fillColor", {}).get("b", 255),
                    z.get("fillColor", {}).get("a", 255),
                )
                z["groups"] = z.get("groups", [{}])[0].get("id")

                for i, point in enumerate(z.get("points", [])):
                    zone_points["zone_id"].append(z["id"])
                    zone_points["latitude"].append(point["y"])
                    zone_points["longitude"].append(point["x"])
                    zone_points["sequence"].append(i)

                for zone_type in z.get("zoneTypes", []):
                    zone_types["zone_id"].append(z["id"])
                    if type(zone_type) == str:
                        zone_types["type_id"].append(zone_type)
                    else:
                        zone_types["type_id"].append(zone_type["id"])

            zone_df = pd.DataFrame(zone)
            zone_types_df = pd.DataFrame(zone_types)
            zone_points_df = pd.DataFrame(zone_points)

            include_cols = [
                "activeFrom",
                "activeTo",
                "comment",
                "displayed",
                "externalReference",
                "fillColor",
                "id",
                "mustIdentifyStops",
                "name",
                "groups",
            ]

            print("Transferring to SQL\n")
            self.__df_to_sql(zone_df[include_cols], "Zones")
            self.__df_to_sql(zone_types_df, "ZoneTypes")
            self.__df_to_sql(zone_points_df, "ZonePoints")
            print("Moving data\n")
            self.__exec_command("{ CALL Geotab.pMoveZones }")

            create_log(self.lc.source_data(), id=log_id, status="success")
            print("Process complete")
        except Exception as e:
            print(traceback.print_exc())
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def status_data_to_sql(self, status_data: list = None):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return None

        if status_data is None:
            if self.client is None:
                self.create_client()
            status_data = self.client.get("StatusData")

        cols = [
            "data",
            "dateTime",
            "device",
            "diagnostic",
            "controller",
            "version",
            "id",
        ]

        status_df = pd.DataFrame(status_data)

        for col in cols:
            if col not in status_df.columns:
                status_df[col] = None

        status_df["controller"] = status_df["controller"].apply(get_id)

        status_df["diagnostic"] = status_df["diagnostic"].apply(get_id)

        status_df["device"] = status_df["device"].apply(get_id)

        status_df["dateTime"] = status_df["dateTime"].apply(self.convert_utc_pt_str)

        data_types = {col_name: types.VARCHAR(length=255) for col_name in status_df}

        self.__df_to_sql(status_df, "StatusData", data_types=data_types)

        self.__exec_command("{ CALL Geotab.pMoveStatusData }")

    def fault_data_to_sql(self, fault_data: list = None):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return col

        if fault_data is None:
            if self.client is None:
                self.create_client()
            fault_data = self.client.get("FaultData")

        cols = [
            "amberWarningLamp",
            "controller",
            "count",
            "dateTime",
            "device",
            "diagnostic",
            "failureMode",
            "faultState",
            "id",
            "malfunctionLamp",
            "protectWarningLamp",
            "redStopLamp",
            "dismissDateTime",
            "dismissUser",
            "sourceAddress",
            "faultStates"
        ]

        fault_df = pd.DataFrame(fault_data)

        # Raise error if there is a new column
        for col in fault_df.columns:
            if col not in cols:
                raise Exception(f"New column ({col}) in data")

        for col in cols:
            if col not in fault_df.columns:
                fault_df[col] = None

        fault_df["controller"] = fault_df["controller"].apply(get_id)

        fault_df["diagnostic"] = fault_df["diagnostic"].apply(get_id)

        fault_df["device"] = fault_df["device"].apply(get_id)

        fault_df["dateTime"] = fault_df["dateTime"].apply(self.convert_utc_pt_str)

        fault_df["dismissDateTime"] = fault_df["dismissDateTime"].apply(
            self.convert_utc_pt_str
        )

        fault_df["dismissUser"] = fault_df["dismissUser"].apply(get_id)

        fault_df["failureMode"] = fault_df["failureMode"].apply(get_id)

        fault_df = fault_df.drop("faultStates",1)

        data_types = {col_name: types.VARCHAR(length=255) for col_name in fault_df}

        self.__df_to_sql(fault_df, "FaultData", data_types=data_types)

        self.__exec_command("{ CALL Geotab.pMoveFaultData }")

    def failure_mode_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="Geotab Failure Mode")

        try:

            failure_data = self.client.get("FailureMode")

            cols = [
                "code",
                "id",
                "name",
                "source",
            ]

            for i, row in enumerate(failure_data):
                if type(row) == str:
                    failure_data[i] = {
                        "code": None,
                        "id": row,
                        "name": self.__parse_id_to_text(row, id_type=""),
                        "source": None,
                    }

            failure_df = pd.DataFrame(failure_data)

            # Raise error if there is a new column
            for col in failure_df.columns:
                if col not in cols:
                    raise Exception(f"New column ({col}) in data")

            for col in cols:
                if col not in failure_df.columns:
                    failure_df[col] = None

            failure_df["source"] = failure_df["source"].apply(
                self.__parse_id_to_text, id_type="Source"
            )

            self.__df_to_sql(failure_df, "FailureMode")

            self.__exec_command("{ CALL Geotab.pMoveFailureMode }")

            create_log(self.lc.source_data(), id=log_id, status="success")

        except Exception as e:
            traceback.print_exc()
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def defect_to_sql(self):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return None

        log_id = create_log(self.lc.source_data(), task="Geotab Defects")

        try:

            data = self.client.get("Defect", meaningless=False)  # Doesn't work with

            cols = [
                "severity",
                "groups",
                "assetType",
                "isDefectList",
                "comments",
                "id",
                "name",
                "reference",
                "parent_id",
            ]

            defects = []

            def unpack_defects(defect_list, parent_id=None):

                for defect in defect_list:
                    children = defect.pop("children", [])
                    if parent_id is None:
                        parent_id = defect["id"]
                    defect["parent_id"] = parent_id
                    defects.append(defect)
                    unpack_defects(children, defect["id"])

            unpack_defects(data)

            defect_df = pd.DataFrame(defects)

            # Raise error if there is a new column
            for col in defect_df.columns:
                if col not in cols:
                    raise Exception(f"New column ({col}) in data")

            for col in cols:
                if col not in defect_df.columns:
                    defect_df[col] = None

            defect_df["groups"] = defect_df["groups"].apply(get_id)

            self.__df_to_sql(defect_df, "Defect")

            self.__exec_command("{ CALL Geotab.pMoveDefects }")

            create_log(self.lc.source_data(), id=log_id, status="success")

        except Exception as e:
            traceback.print_exc()
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def dvir_to_sql(self, dvir: list = None):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return col

        if dvir is None:
            if self.client is None:
                self.create_client()
            dvir = self.client.get("DVIRLog")
            
            df = pd.json_normalize(dvir)
            # df.to_csv('dvirLog.txt', header=True, index=False)

        dvir_cols = [
            "id",
            "certifiedBy",
            "certifyDate",
            "certifyRemark",
            "defects",
            "dVIRDefects",
            "device",
            "driverRemark",
            "isSafeToOperate",
            "repairDate",
            "repairedBy",
            "dateTime",
            "driver",
            "logType",
            "defectList",
            "authorityName",
            "authorityAddress",
            "isInspectedByDriver",
            "isRejected",
            "version",
            "trailer",
            "location",
            "latitude",
            "longitude",
            "speed",
        ]

        dvir_defects = []
        dvir_defect_remarks = []

        for i, row in enumerate(dvir):
            dvir[i]["certifiedBy"] = get_id(row.get("certifiedBy"))
            dvir[i]["certifyDate"] = self.convert_utc_pt_str(row.get("certifyDate"))
            dvir[i]["device"] = get_id(row.get("device"))
            dvir[i]["repairDate"] = self.convert_utc_pt_str(row.get("repairDate"))
            dvir[i]["repairedBy"] = get_id(row.get("repairedBy"))
            dvir[i]["dateTime"] = self.convert_utc_pt_str(row.get("dateTime"))
            dvir[i]["driver"] = get_id(row.get("driver"))
            dvir[i]["trailer"] = get_id(row.get("trailer"))
            dvir[i]["latitude"] = row.get("location", {}).get("y")
            dvir[i]["longitude"] = row.get("location", {}).get("s")
            dvir[i]["defectList"] = row.get("defectList", {}).get("defectList")

            # DVIR Defects
            for defect in row.get("dVIRDefects", []):
                dvir_defects.append(
                    {
                        "dvir_id": row["id"],
                        "dvir_defect_id": defect["id"],
                        "defect": get_id(defect["defect"]),
                        "repairStatus": defect.get("repairStatus"),
                        "repairDateTime": self.convert_utc_pt_str(
                            defect.get("repairDateTime")
                        ),
                        "repairUser": get_id(defect.get("repairUser")),
                    }
                )
                for remark in defect.get("defectRemarks", []):
                    dvir_defect_remarks.append(
                        {
                            "dvir_defect_id": get_id(remark["dVIRDefect"]),
                            "dvir_defect_remark_id": remark["id"],
                            "user": get_id(remark["user"]),
                            "remark": remark["remark"],
                            "dateTime": self.convert_utc_pt_str(remark["dateTime"]),
                        }
                    )

        dvir_df = pd.DataFrame(dvir)
        defect_df = pd.DataFrame(dvir_defects)
        remark_df = pd.DataFrame(dvir_defect_remarks)

        # Raise error if there is a new column
        for col in dvir_df.columns:
            if col not in dvir_cols:
                raise Exception(f"New column ({col}) in data")

        for col in dvir_cols:
            if col not in dvir_df.columns:
                dvir_df[col] = None

        include_cols = [
            "id",
            "certifiedBy",
            "certifyDate",
            "certifyRemark",
            "device",
            "driverRemark",
            "isSafeToOperate",
            "repairDate",
            "repairedBy",
            "dateTime",
            "driver",
            "logType",
            "defectList",
            "authorityName",
            "authorityAddress",
            "isInspectedByDriver",
            "isRejected",
            "version",
            "trailer",
            "latitude",
            "longitude",
            "speed",
        ]

        self.__df_to_sql(dvir_df[include_cols], "DVIR")
        self.__df_to_sql(defect_df, "DVIRDefects")
        self.__df_to_sql(remark_df, "DVIRDefectRemarks")

        self.__exec_command("{ CALL Geotab.pMoveDVIR }")

    def driver_change_to_sql(self, changes: list = None):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return col

        if changes is None:
            if self.client is None:
                self.create_client()
            changes = self.client.get("DriverChange")

        cols = ["type", "dateTime", "device", "driver", "version", "id"]

        df = pd.DataFrame(changes)

        # Raise error if there is a new column
        for col in df.columns:
            if col not in cols:
                raise Exception(f"New column ({col}) in data")

        for col in df:
            if col not in df.columns:
                df[col] = None

        df["driver"] = df["driver"].apply(get_id)
        df["device"] = df["device"].apply(get_id)
        df["dateTime"] = df["dateTime"].apply(self.convert_utc_pt_str)

        self.__df_to_sql(df, "DriverChanges")

        self.__exec_command("{ CALL Geotab.pMoveDriverChanges }")

    def trips_to_sql(self, trips: list = None):
        def get_id(col):
            if type(col) == dict:
                return col["id"]
            return col

        def get_key(col, dict_key: str):
            if type(col) == dict:
                return col.get(dict_key)
            return None

        if trips is None:
            if self.client is None:
                self.create_client()
            trips = self.client.get("Trip")

        cols = [
            "afterHoursDistance",
            "afterHoursDrivingDuration",
            "afterHoursEnd",
            "afterHoursStart",
            "afterHoursStopDuration",
            "averageSpeed",
            "distance",
            "drivingDuration",
            "idlingDuration",
            "isSeatBeltOff",
            "maximumSpeed",
            "nextTripStart",
            "speedRange1",
            "speedRange1Duration",
            "speedRange2",
            "speedRange2Duration",
            "speedRange3",
            "speedRange3Duration",
            "start",
            "stop",
            "stopDuration",
            "stopPoint",
            "workDistance",
            "workDrivingDuration",
            "workStopDuration",
            "device",
            "driver",
            "id",
            "engineHours",
        ]

        df = pd.DataFrame(trips)
        
        # Raise error if there is a new column
        for col in df.columns:
            if col not in cols:
                raise Exception(f"New column ({col}) in data")

        for col in df:
            if col not in df.columns:
                df[col] = None

        df["afterHoursDistance"] = df["afterHoursDistance"].apply(km_to_mi)
        df["afterHoursDrivingDuration"] = df["afterHoursDrivingDuration"].apply(
            self.duration_to_seconds
        )
        df["afterHoursStopDuration"] = df["afterHoursStopDuration"].apply(
            self.duration_to_seconds
        )
        df["averageSpeed"] = df["averageSpeed"].apply(km_to_mi)
        df["distance"] = df["distance"].apply(km_to_mi)
        df["drivingDuration"] = df["drivingDuration"].apply(self.duration_to_seconds)
        df["idlingDuration"] = df["idlingDuration"].apply(self.duration_to_seconds)
        df["maximumSpeed"] = df["maximumSpeed"].apply(km_to_mi)
        df["nextTripStart"] = df["nextTripStart"].apply(self.convert_utc_pt_str)
        df["speedRange1Duration"] = df["speedRange1Duration"].apply(
            self.duration_to_seconds
        )
        df["speedRange2Duration"] = df["speedRange2Duration"].apply(
            self.duration_to_seconds
        )
        df["speedRange3Duration"] = df["speedRange3Duration"].apply(
            self.duration_to_seconds
        )
        df["start"] = df["start"].apply(self.convert_utc_pt_str)
        df["stop"] = df["stop"].apply(self.convert_utc_pt_str)
        df["stopDuration"] = df["stopDuration"].apply(self.duration_to_seconds)
        df["stopPointLatitude"] = df["stopPoint"].apply(get_key, dict_key="y")
        df["stopPointLongitude"] = df["stopPoint"].apply(get_key, dict_key="x")
        df["workDistance"] = df["workDistance"].apply(km_to_mi)
        df["workDrivingDuration"] = df["workDrivingDuration"].apply(
            self.duration_to_seconds
        )
        df["workStopDuration"] = df["workStopDuration"].apply(self.duration_to_seconds)
        df["device"] = df["device"].apply(get_id)
        df["driver"] = df["driver"].apply(get_id)

        df = df.drop(["stopPoint"], axis=1)

        self.__df_to_sql(df, "Trips")

        self.__exec_command("{ CALL Geotab.pMoveTrips }")
