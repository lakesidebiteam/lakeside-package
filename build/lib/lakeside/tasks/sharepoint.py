from pypyodbc import Cursor
import requests
import msal
from pprint import pprint
import pandas as pd
import traceback
from datetime import datetime, timedelta
import dateutil
import re

from lakeside.connection.sql_server import LakesideConnections
from lakeside.tools.phone import clean_phone
from lakeside.tools.lists import first_item
from lakeside.log import create_log
from lakeside.tools.dates import convert_str_utc_pt_str
from lakeside.tools.text import strip_tags


class SharePoint:
    def __init__(self) -> None:
        self.lc = LakesideConnections(app="SharePoint Integration")

        self.__get_config()

        self.__token = None
        self.__token_exp = datetime.now()

    def __get_config(self):

        con = self.lc.source_data()
        cursor = con.cursor()

        try:

            command = """
                SELECT
                    Authority,
                    ClientID,
                    Secret
                FROM SharePoint.API
                """

            cursor.execute(command)

            data = cursor.fetchone()

        except Exception as e:
            raise e
        finally:
            con.close()

        self.authority, self.client_id, self.secret = tuple(data)

    def __aquire_token(self):

        if "self.authority" not in globals():
            self.__get_config()

        app = msal.ConfidentialClientApplication(
            self.client_id,
            authority=self.authority,
            client_credential=self.secret,
        )

        result = None

        result = app.acquire_token_silent(
            ["https://graph.microsoft.com/.default"], account=None
        )

        if not result:
            result = app.acquire_token_for_client(
                scopes=["https://graph.microsoft.com/.default"]
            )

        if "access_token" in result:
            self.__token = result["access_token"]
            try:
                seconds = int(result.get("expires_in")) - 30
            except:
                seconds = 3570

            self.__token_exp = datetime.now() + timedelta(seconds=seconds)
        else:
            raise Exception(f"No Access Token: {result}")

    def __df_to_sql(self, df, name):
        self.lc.df_to_sql(
            self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage"),
            "SharePoint",
            df,
            name,
        )

    def __exec_command(self, command, values=None):
        self.lc.execute_command(
            self.lc.source_data(),
            command,
            values,
        )

    def get_token(self):

        if datetime.now() >= self.__token_exp:
            self.__aquire_token()

        return self.__token

    def get_data(self, url):

        header = {"Authorization": "Bearer " + self.get_token()}

        data = requests.get(url, headers=header).json()

        return data

    def iterate_pages(self, url):

        data = self.get_data(url)

        values = data["value"]

        if "@odata.nextLink" in data.keys():
            values.extend(self.iterate_pages(data["@odata.nextLink"]))

        return values

    def users_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="SharePoint Users")

        try:

            data = self.iterate_pages("https://graph.microsoft.com/v1.0/users")

            df = pd.DataFrame(data)

            df["businessPhones"] = (
                df["businessPhones"].apply(first_item).apply(clean_phone)
            )
            df["mobilePhone"] = df["mobilePhone"].apply(clean_phone)

            self.__df_to_sql(df, "Users")

            self.__exec_command("{ CALL SharePoint.pMoveUsers }")

            create_log(self.lc.source_data(), id=log_id, status="success")
        except:
            traceback.print_exc()
            create_log(
                self.lc.source_data,
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def sites_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="SharePoint Sites")

        try:

            data = self.iterate_pages(
                "https://graph.microsoft.com/v1.0/sites/root/sites?search=*"
            )

            for row in data:
                row["createdDateTime"] = convert_str_utc_pt_str(
                    row.get("createdDateTime")
                )
                if row.get("lastModifiedDateTime", "0").startswith("0"):
                    row["lastModifiedDateTime"] = None
                else:
                    row["lastModifiedDateTime"] = convert_str_utc_pt_str(
                        row.get("lastModifiedDateTime")
                    )
                row.pop("root", None)
                row.pop("siteCollection", None)

            df = pd.DataFrame(data)

            self.__df_to_sql(df, "Sites")

            self.__exec_command("{ CALL SharePoint.pMoveSites }")

            create_log(self.lc.source_data(), id=log_id, status="success")
        except:
            traceback.print_exc()
            create_log(
                self.lc.source_data,
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def __site_columns_to_sql(self):
        log_id = create_log(self.lc.source_data(), task="SharePoint Site List Columns")

        try:

            con = self.lc.source_data()
            cursor = con.cursor()

            command = """
                    Select

                        A.SiteID, ListID, B.DisplayName, A.ListDisplayName

                    From SharePoint.Lists as A
                    LEFT JOIN SharePoint.Sites as B
                        ON A.SiteID = B.SiteID
                    ORDER BY B.DisplayName, A.ListDisplayName
            """
            cursor.execute(command)

            lists = cursor.fetchall()

            data = []

            for list in lists:
                print(f"{list[2]}: {list[3]}")

                url = f"https://graph.microsoft.com/v1.0/sites/{list[0]}/lists/{list[1]}/columns"
                results = self.iterate_pages(url)
                for row in results:
                    row["siteId"] = list[0]
                    row["listId"] = list[1]

                data.extend(results)

            df = pd.DataFrame(data)

            include_cols = [
                "id",
                "name",
                "displayName",
                "siteId",
                "listId",
            ]

            self.__df_to_sql(df[include_cols], "ListColumns")
            self.__exec_command("{ CALL SharePoint.pMoveListColumns }")

            create_log(self.lc.source_data(), id=log_id, status="success")
        except:
            traceback.print_exc()
            create_log(
                self.lc.source_data,
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def site_lists_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="SharePoint Site Lists")

        try:

            con = self.lc.source_data()
            cursor = con.cursor()
            command = "Select SiteID, SiteName from SharePoint.Sites"
            cursor.execute(command)
            sites = cursor.fetchall()

            data = []
            for site in sites:
                print(site[1])
                url = f"https://graph.microsoft.com/v1.0/sites/{site[0]}/lists"
                results = self.iterate_pages(url)
                data.extend(results)

            for row in data:
                row["createdDateTime"] = convert_str_utc_pt_str(
                    row.get("createdDateTime")
                )
                row["lastModifiedDateTime"] = convert_str_utc_pt_str(
                    row.get("lastModifiedDateTime")
                )
                row["createdByEmail"] = (
                    row.get("createdBy", {}).get("user", {}).get("email")
                )
                row["createdByUserID"] = (
                    row.get("createdBy", {}).get("user", {}).get("id")
                )
                row["createdBy"] = (
                    row.get("createdBy", {}).get("user", {}).get("displayName")
                )
                row["lastModifiedByEmail"] = (
                    row.get("lastModifiedBy", {}).get("user", {}).get("email")
                )
                row["lastModifiedByUserID"] = (
                    row.get("lastModifiedBy", {}).get("user", {}).get("id")
                )
                row["lastModifiedBy"] = (
                    row.get("lastModifiedBy", {}).get("user", {}).get("displayName")
                )
                row["siteId"] = row.get("parentReference", {}).get("siteId")
                row.pop("parentReference", None)
                row["contentTypesEnabled"] = row.get("list", {}).get(
                    "contentTypesEnabled"
                )
                row["contentTypesEnabled"] = row.get("list", {}).get("hidden")
                row["contentTypesEnabled"] = row.get("list", {}).get("template")
                row.pop("list", None)

            df = pd.DataFrame(data)

            self.__df_to_sql(df, "Lists")

            self.__exec_command("{ CALL SharePoint.pMoveLists }")

            print("Getting Columns for Lists")

            self.__site_columns_to_sql()

            create_log(self.lc.source_data(), id=log_id, status="success")
        except:
            traceback.print_exc()
            create_log(
                self.lc.source_data,
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def __get_list_columns(self, site_id, list_id):
        con = self.lc.source_data()
        cursor = con.cursor()

        command = "SELECT ColumnName FROM SharePoint.ListColumns WHERE SiteID = ? and ListID = ?"

        cursor.execute(command, [site_id, list_id])

        data = cursor.fetchall()

        col_names = [i[0] for i in data]

        return ",".join(col_names)

    def get_list_data(self, site_id, list_id):

        columns = self.__get_list_columns(site_id, list_id)

        url = f"https://graph.microsoft.com/v1.0/sites/{site_id}/lists/{list_id}/items?expand=fields(select={columns})"

        data = self.iterate_pages(url)

        return data

    def unpack_user(self, user):
        """Takes user dictionary and returns a tuple (display_name, email, id)"""

        if type(user) != dict:
            return None

        display_name = user.get("user", {}).get("displayName")
        email = user.get("user", {}).get("email")
        id = user.get("user", {}).get("id")

        return (display_name, email, id)

    def monroe_paving_calendar_to_sql(self):
        log_id = create_log(
            self.lc.source_data(), task="SharePoint Monroe Paving Calendar"
        )

        try:

            site_id = "lakesideindustries.sharepoint.com,1ba7bcb9-95dd-48fa-9600-0d9e93f03d83,235552d0-4c10-475f-9a83-8710f81e82f9"
            list_id = "022c2259-4b2b-4836-9c2d-0fb68f68ccfe"

            data = self.get_list_data(site_id, list_id)

            crew = {
                "item_id": [],
                "crew": [],
            }
            demo_crew = {
                "item_id": [],
                "crew": [],
            }
            equipment = {
                "item_id": [],
                "equipment": [],
            }
            grinding = {
                "item_id": [],
                "grinding": [],
            }
            trucking = {
                "item_id": [],
                "trucking": [],
            }
            type_of_work = {
                "item_id": [],
                "type_of_work": [],
            }
            truck_load_out = {
                "item_id": [],
                "col_1": [],
                "col_2": [],
                "col_3": [],
                "col_4": [],
                "col_5": [],
                "col_6": [],
            }

            for row in data:
                row.pop("@odata.etag", None)
                (
                    row["createdBy"],
                    row["createdByEmail"],
                    row["createdById"],
                ) = self.unpack_user(row.get("createdBy"))
                row["createdDateTime"] = convert_str_utc_pt_str(
                    row.get("createdDateTime")
                )
                row.pop("eTag", None)
                fields = row.get("fields", {})
                fields.pop("@odata.etag", None)
                row["Attachments"] = fields.pop("Attachments", None)
                row["Author"] = fields.pop("Author", None)
                row["Category"] = fields.pop("Category", None)
                fields.pop("Created", None)
                for crw in fields.pop("Crew", []):
                    if crw != "-":
                        crew["item_id"].append(fields["id"])
                        crew["crew"].append(crw)
                for crw in fields.pop("Dave_x0020_Baker_x0020_Crew", []):
                    if crw != "-":
                        crew["item_id"].append(fields["id"])
                        crew["crew"].append(crw)
                for crw in fields.pop("Jeff_x0020_Conway_x0020_Crew", []):
                    if crw != "-":
                        crew["item_id"].append(fields["id"])
                        crew["crew"].append(crw)
                for crw in fields.pop("Mark_x0020_Murphy_x0020_Crew", []):
                    if crw != "-":
                        crew["item_id"].append(fields["id"])
                        crew["crew"].append(crw)
                row["Tons"] = fields.pop("Daily_x0020_Tons", None)
                for crw in fields.pop("Demo_x0020_Crew", []):
                    if crw != "-":
                        demo_crew["item_id"].append(fields["id"])
                        demo_crew["crew"].append(crw)
                row["Description"] = re.sub(
                    r"[^\x00-\x7F]+",
                    " ",
                    strip_tags(fields.pop("Description", "").replace("<br>", "\n\n")),
                ).strip()
                row["Edit"] = fields.pop("Edit", None)
                row["Editor"] = fields.pop("Editor", None)
                row["EndDate"] = convert_str_utc_pt_str(fields.pop("EndDate", None), dateutil.tz.tzutc() if fields.get('fAllDayEvent') else None)
                for equip in fields.pop("Equipment", []):
                    if equip != "-":
                        equipment["item_id"].append(fields["id"])
                        equipment["equipment"].append(equip)
                row["EventDate"] = convert_str_utc_pt_str(fields.pop("EventDate", None), dateutil.tz.tzutc() if fields.get('fAllDayEvent') else None)
                row["Flagging"] = fields.pop("Flagging", None)
                fields.pop("FolderChildCount", None)
                for grind in fields.pop("Grinding_x0020_Sub", []):
                    if grind != "-":
                        grinding["item_id"].append(fields["id"])
                        grinding["grinding"].append(grind)
                fields.pop("ItemChildCount", None)
                fields.pop("Job_x0020_Title_x0020_with_x0020", None)
                row["LinkTitle"] = fields.pop("LinkTitle", None)
                fields.pop("LinkTitleNoMenu", None)
                row["Location"] = fields.pop("Location", None)
                row["MixNumber"] = fields.pop("Mix_x0020_Number_x002f_Type", None)
                row["Modified"] = convert_str_utc_pt_str(fields.pop("Modified", None))
                row["Owner"] = fields.pop("Owner", None)
                row["ProjectTons"] = fields.pop("Project_x0020_Tons", None)
                row["Superintendent"] = fields.pop("Superintendent", None)
                row["Title"] = fields.pop("Title", None)
                row["TruckLoadOut"] = fields.pop(
                    "Truck_x0020_Load_x0020_out_x0020", None
                )
                if row["TruckLoadOut"] is not None:
                    lines = row["TruckLoadOut"].split("\r\n")
                    for line in lines:
                        cells = line.split()
                        cols = [None for i in range(6)]
                        for i in range(len(cells)):
                            if i < 6:
                                cols[i] = cells[i].encode("ascii", errors="ignore")

                        truck_load_out["item_id"].append(fields["id"])
                        truck_load_out["col_1"].append(cols[0])
                        truck_load_out["col_2"].append(cols[1])
                        truck_load_out["col_3"].append(cols[2])
                        truck_load_out["col_4"].append(cols[3])
                        truck_load_out["col_5"].append(cols[4])
                        truck_load_out["col_6"].append(cols[5])
                row["TruckLoadOut"] = (
                    None
                    if row["TruckLoadOut"] is None
                    else row["TruckLoadOut"].encode("ascii", errors="ignore")
                )

                for truck in fields.pop("Trucking", []):
                    if truck != "-":
                        trucking["item_id"].append(fields["id"])
                        trucking["trucking"].append(truck)
                for tow in fields.pop("Type_x0020_of_x0020_Work", []):
                    if tow != "-":
                        type_of_work["item_id"].append(fields["id"])
                        type_of_work["type_of_work"].append(tow)
                fields.pop("_ComplianceFlags", None)
                fields.pop("_ComplianceTag", None)
                fields.pop("_ComplianceTagUserId", None)
                fields.pop("_ComplianceTagWrittenTime", None)
                fields.pop("_UIVersionString", None)
                row["AllDayEvent"] = fields.pop("fAllDayEvent", None)
                row["Recurrence"] = fields.pop("fRecurrence", None)
                fields.pop("id", None)
                row.pop("fields@odata.context", None)
                (
                    row["lastModifiedBy"],
                    row["lastModifiedByEmail"],
                    row["lastModifiedById"],
                ) = self.unpack_user(row.get("lastModifiedBy"))
                row["lastModifiedDateTime"] = convert_str_utc_pt_str(
                    row.get("lastModifiedDateTime", None)
                )
                row.pop("parentReference", None)
                fields.pop("ContentType", None)
                row.pop("contentType", None)
                row["LoaderOperator"] = fields.pop("Loader_x0020_Operator", None)
                row["RockType"] = fields.pop("Rock_x0020_Type_x002f_Tons", None)
                row.pop("fields")

            df = pd.DataFrame(data)

            self.__df_to_sql(
                df,
                "MonroePavingSchedule",
            )

            crew_df = pd.DataFrame(crew)
            demo_df = pd.DataFrame(demo_crew)
            equipment_df = pd.DataFrame(equipment)
            grinding_df = pd.DataFrame(grinding)
            trucking_df = pd.DataFrame(trucking)
            type_df = pd.DataFrame(type_of_work)
            truck_lo_df = pd.DataFrame(truck_load_out)

            self.__df_to_sql(
                crew_df,
                "MonroePavingScheduleCrew",
            )
            self.__df_to_sql(
                demo_df,
                "MonroePavingScheduleDemoCrew",
            )
            self.__df_to_sql(
                equipment_df,
                "MonroePavingScheduleEquipment",
            )
            self.__df_to_sql(
                grinding_df,
                "MonroePavingScheduleGrinding",
            )
            self.__df_to_sql(
                trucking_df,
                "MonroePavingScheduleTrucking",
            )
            self.__df_to_sql(
                type_df,
                "MonroePavingScheduleType",
            )
            self.__df_to_sql(
                truck_lo_df,
                "MonroePavingScheduleTruckLoadOut",
            )

            self.__exec_command("{ CALL SharePoint.pMoveMonroePavingSchedule }")

            create_log(self.lc.source_data(), log_id, "success")

        except:
            traceback.print_exc()
            create_log(self.lc.source_data(), log_id, "failure", traceback.format_exc())
