import phonenumbers as pn

def clean_phone(number):
    try:
        number = pn.parse(number, "US")
        cleaned = pn.format_number(number, pn.PhoneNumberFormat.NATIONAL)
    except:
        cleaned = number
    return cleaned