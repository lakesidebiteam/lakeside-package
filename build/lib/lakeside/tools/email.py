from distutils.util import subst_vars
from email.message import EmailMessage
from email.headerregistry import Address
from email.utils import make_msgid
from email.mime.application import MIMEApplication
import smtplib

from lakeside.tools.resource import get_resource_path


class Email:
    def __init__(self, subject: str = None) -> None:
        """Creates and sends email

        Keyword Arguements
        subject - Email message subject. Can also be set by set_subject()
        """

        self.subject = subject
        self.recipients = []
        self.send_address = {
            "name": "No Reply",
            "username": "no-reply",
            "domain": "lakesideindustries.com",
        }
        self.greeting = "Hello {name},"

        self.plain_message = ""
        self.html_message = ""

        self.preview_text = ''

        self.style = ""

        self.attachments = []

        self.__smtp = "mail.lakesideindustries.com"

        self.__html_wrapper = """
            <html>
                <head>
    
                </head>
                <style>
                    .header {{
                        background: #144e8c;
                        height: 50px;
                        padding: 5px;
                        padding-left: 10px;
                    }}
                    .footer {{
                        background: #9EBFE1;
                        height: 150px;
                        width: 100%;
                    }}
                    img {{
                        padding: 10px;
                    }}
                    .table {{
                        width: 100%;
                    }}
                    p {{
                        font-family: sans-serif;
                        font-size: 1em;
                    }}
                    .greeting {{
                        font-size: 1.25em;
                    }}
                    body {{
                        background: white;
                    }}
                    th {{
                        text-align: left;
                    }}
                    
                </style>
                <body>
                    <table class="table">
                        <tr class="header">
                            
                            <td>
                                <img src="cid:{logo_cid}" alt="Lakeside Logo">
                            </td>
                        </tr>
                    </table>
                    <p class="greeting">{greeting},</p><br>
                    {message}
                </body>
            </html>
            """

        self.__html_wrapper = """
            <!doctype html>
            <html>
            <head>
                <meta name="viewport" content="width=device-width" />
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title>Simple Transactional Email</title>
                <style>
                /* -------------------------------------
                    GLOBAL RESETS
                ------------------------------------- */
                
                /*All the styling goes here*/
                
                img {{
                    border: none;
                    -ms-interpolation-mode: bicubic;
                    max-width: 100%; 
                }}

                body {{
                    background-color: #f6f6f6;
                    font-family: sans-serif;
                    -webkit-font-smoothing: antialiased;
                    font-size: 14px;
                    line-height: 1.4;
                    margin: 0;
                    padding: 0;
                    -ms-text-size-adjust: 100%;
                    -webkit-text-size-adjust: 100%; 
                }}

                table {{
                    border-collapse: separate;
                    mso-table-lspace: 0pt;
                    mso-table-rspace: 0pt;
                    width: 100%; }}
                    table td {{
                    font-family: sans-serif;
                    font-size: 14px;
                    vertical-align: top; 
                }}

                .detailTable {{
                    font-size: 8px;
                    border: 1px solid black;
                    border-collapse: collapse;
                }}
                

                /* -------------------------------------
                    BODY & CONTAINER
                ------------------------------------- */

                .body {{
                    background-color: #f6f6f6;
                    width: 100%; 
                }}

                /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
                .container {{
                    display: block;
                    margin: 0 auto !important;
                    /* makes it centered */
                    max-width: 580px;
                    padding: 10px;
                    width: 580px; 
                }}

                /* This should also be a block element, so that it will fill 100% of the .container */
                .content {{
                    box-sizing: border-box;
                    display: block;
                    margin: 0 auto;
                    max-width: 580px;
                    padding: 10px; 
                }}

                /* -------------------------------------
                    HEADER, FOOTER, MAIN
                ------------------------------------- */
                .main {{
                    background: #ffffff;
                    border-radius: 3px;
                    width: 100%; 
                }}

                .wrapper {{
                    box-sizing: border-box;
                    padding: 20px; 
                }}

                .content-block {{
                    padding-bottom: 10px;
                    padding-top: 10px;
                }}

                .footer {{
                    clear: both;
                    margin-top: 10px;
                    text-align: center;
                    width: 100%; 
                }}
                    .footer td,
                    .footer p,
                    .footer span,
                    .footer a {{
                    color: #999999;
                    font-size: 12px;
                    text-align: center; 
                }}

                /* -------------------------------------
                    TYPOGRAPHY
                ------------------------------------- */
                h1,
                h2,
                h3,
                h4 {{
                    color: #000000;
                    font-family: sans-serif;
                    font-weight: 400;
                    line-height: 1.4;
                    margin: 0;
                    margin-bottom: 30px; 
                }}

                h1 {{
                    font-size: 35px;
                    font-weight: 300;
                    text-align: center;
                    text-transform: capitalize; 
                }}

                p,
                ul,
                ol {{
                    font-family: sans-serif;
                    font-size: 14px;
                    font-weight: normal;
                    margin: 0;
                    margin-bottom: 15px; 
                }}
                    p li,
                    ul li,
                    ol li {{
                    list-style-position: inside;
                    margin-left: 5px; 
                }}

                a {{
                    color: #3498db;
                    text-decoration: underline; 
                }}

                /* -------------------------------------
                    BUTTONS
                ------------------------------------- */
                .btn {{
                    box-sizing: border-box;
                    width: 100%; }}
                    .btn > tbody > tr > td {{
                    padding-bottom: 15px; }}
                    .btn table {{
                    width: auto; 
                }}
                    .btn table td {{
                    background-color: #ffffff;
                    border-radius: 5px;
                    text-align: center; 
                }}
                    .btn a {{
                    background-color: #ffffff;
                    border: solid 1px #3498db;
                    border-radius: 5px;
                    box-sizing: border-box;
                    color: #3498db;
                    cursor: pointer;
                    display: inline-block;
                    font-size: 14px;
                    font-weight: bold;
                    margin: 0;
                    padding: 12px 25px;
                    text-decoration: none;
                    text-transform: capitalize; 
                }}

                .btn-primary table td {{
                    background-color: #3498db; 
                }}

                .btn-primary a {{
                    background-color: #3498db;
                    border-color: #3498db;
                    color: #ffffff; 
                }}

                /* -------------------------------------
                    OTHER STYLES THAT MIGHT BE USEFUL
                ------------------------------------- */
                .last {{
                    margin-bottom: 0; 
                }}

                .first {{
                    margin-top: 0; 
                }}

                .align-center {{
                    text-align: center; 
                }}

                .align-right {{
                    text-align: right; 
                }}

                .align-left {{
                    text-align: left; 
                }}

                .clear {{
                    clear: both; 
                }}

                .mt0 {{
                    margin-top: 0; 
                }}

                .mb0 {{
                    margin-bottom: 0; 
                }}

                .preheader {{
                    color: transparent;
                    display: none;
                    height: 0;
                    max-height: 0;
                    max-width: 0;
                    opacity: 0;
                    overflow: hidden;
                    mso-hide: all;
                    visibility: hidden;
                    width: 0; 
                }}

                .powered-by a {{
                    text-decoration: none; 
                }}

                hr {{
                    border: 0;
                    border-bottom: 1px solid #f6f6f6;
                    margin: 20px 0; 
                }}

                /* -------------------------------------
                    RESPONSIVE AND MOBILE FRIENDLY STYLES
                ------------------------------------- */
                @media only screen and (max-width: 620px) {{
                    table[class=body] h1 {{
                    font-size: 28px !important;
                    margin-bottom: 10px !important; 
                    }}
                    table[class=body] p,
                    table[class=body] ul,
                    table[class=body] ol,
                    table[class=body] td,
                    table[class=body] span,
                    table[class=body] a {{
                    font-size: 16px !important; 
                    }}
                    table[class=body] .wrapper,
                    table[class=body] .article {{
                    padding: 10px !important; 
                    }}
                    table[class=body] .content {{
                    padding: 0 !important; 
                    }}
                    table[class=body] .container {{
                    padding: 0 !important;
                    width: 100% !important; 
                    }}
                    table[class=body] .main {{
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important; 
                    }}
                    table[class=body] .btn table {{
                    width: 100% !important; 
                    }}
                    table[class=body] .btn a {{
                    width: 100% !important; 
                    }}
                    table[class=body] .img-responsive {{
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important; 
                    }}
                }}

                /* -------------------------------------
                    PRESERVE THESE STYLES IN THE HEAD
                ------------------------------------- */
                @media all {{
                    .ExternalClass {{
                    width: 100%; 
                    }}
                    .ExternalClass,
                    .ExternalClass p,
                    .ExternalClass span,
                    .ExternalClass font,
                    .ExternalClass td,
                    .ExternalClass div {{
                    line-height: 100%; 
                    }}
                    .apple-link a {{
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important; 
                    }}
                    #MessageViewBody a {{
                    color: inherit;
                    text-decoration: none;
                    font-size: inherit;
                    font-family: inherit;
                    font-weight: inherit;
                    line-height: inherit;
                    }}
                    .btn-primary table td:hover {{
                    background-color: #34495e !important; 
                    }}
                    .btn-primary a:hover {{
                    background-color: #34495e !important;
                    border-color: #34495e !important; 
                    }} 
                }}

                

                </style>
            </head>
            <body class="">
                <span class="preheader">{preview_text}</span>
                
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
                <tr>
                    <td>&nbsp;</td>
                    <td class="container">
                    
                    <div class="content">
    
                                    
                        <!-- START CENTERED WHITE CONTAINER -->
                        <table role="presentation" class="main">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                <td>
                                    <img src="cid:{logo_cid}" alt="Lakeside Logo" width="150"> <br>
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <br>{greeting} <br>
                                    <br>{message}
                                </td>
                                </tr>
                            </table>
                            </td>
                        </tr>                        

                        <!-- END MAIN CONTENT AREA -->
                        </table>
                        <!-- END CENTERED WHITE CONTAINER -->

                        <!-- START FOOTER -->
                        <div class="footer">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                            <td class="content-block">
                                <span class="apple-link">Lakeside Industries</span>
 
                            </td>
                            </tr>
                            
                        </table>
                        </div>
                        <!-- END FOOTER -->

                    </div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </table>
            </body>
            </html>"""

    def add_recipient(self, name: str, username: str, domain: str):
        """Adds a recipient to the list of recipients to receive an email

        Keyword Arguements
        name - Friendly name of recipient (e.g. John Doe)
        username - Username part of email address (e.g. john.doe)
        domain - Email domain (e.g. test.com)
        """

        self.recipients.append(
            {
                "name": name,
                "username": username,
                "domain": domain,
            }
        )

    def add_attachment(self, name, file):
        self.attachments.append([name, file])

    def set_from_address(self, name: str, username: str, domain: str):
        """Overwrite default send from address (no-reply@lakesideindustries.com)

        Keyword Arguements
        name - Friendly name of recipient (e.g. John Doe)
        username - Username part of email address (e.g. john.doe)
        domain - Email domain (e.g. test.com)
        """
        self.send_address["name"] = name
        self.send_address["username"] = username
        self.send_address["domain"] = domain

    def set_subject(self, subject: str):
        """Sets the email subject

        Keyword Arguements
        subject - Subject to use for email
        """

        self.subject = subject

    def process_email(self):

        for recipient in self.recipients:

            msg = EmailMessage()

            msg["Subject"] = self.subject
            msg["From"] = Address(
                self.send_address["name"],
                self.send_address["username"],
                self.send_address["domain"],
            )
            msg["To"] = Address(
                recipient["name"],
                recipient["username"],
                recipient["domain"],
            )

            greeting = self.__create_greeting(recipient["name"])

            plain_message = self.__construct_plain_message(greeting, self.plain_message)

            logo_cid = make_msgid()

            html_message = self.__construct_html_message(
                greeting, self.__html_wrapper, self.html_message, logo_cid
            )

            msg.set_content(plain_message)
            msg.add_alternative(html_message, subtype="html")

            with open(get_resource_path("lakeside_logo.png"), "rb") as img:
                msg.get_payload()[1].add_related(
                    img.read(), "image", "png", cid=logo_cid
                )

            for file in self.attachments:
                part = MIMEApplication(file[1], Name=file[0])

                part["Content-Disposition"] = f'attachment; filename="{file[0]}"'

                msg.attach(part)

            with smtplib.SMTP(self.__smtp) as s:
                s.send_message(msg)

            print(f'Sent email to {recipient["name"]}')

    def __create_greeting(self, name):
        return self.greeting.format(name=name)

    def set_plain_message(self, message: str):
        """Set the plain text version of the message

        Keyword Arguements
        message - plain text message to send
        """

        self.plain_message = message

    def set_html_message(self, message: str):
        """Set the html version of the message

        Keyword Arguements
        message - html to include in message
        """

        self.html_message = message

    def __construct_plain_message(self, greeting: str, text: str):
        message = greeting + ","
        message += "\n"
        message += text
        return message

    def __construct_html_message(
        self, greeting: str, wrapper: str, text: str, logo_cid
    ):

        message = wrapper.format(
            logo_cid=logo_cid[1:-1], greeting=greeting, message=text, preview_text = self.preview_text
        )

        return message
