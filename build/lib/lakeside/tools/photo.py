from PIL import Image
import os


def open_image(path):
    return Image.open(path)


def resize_image(image, max_size = 400):
    size = image.size

    if max(size) <= max_size:
        return image
    
    ratio = float(max_size) / max(size)
    new_size = tuple([int(x*ratio) for x in size])

    image = image.resize(new_size, Image.ANTIALIAS)

    new_image = Image.new("RGB", new_size)
    new_image.paste(image, (0, 0))

    return new_image


def resize_images_in_path(path, max_size=400):

    dirs = os.listdir(path)

    for item in dirs:
        file_path = os.path.join(path, item)
        if os.path.isfile(file_path):
            f, e = os.path.splitext(file_path)

            try:
                im = open_image(file_path)
                if not im.verify():
                    raise "Invalid Image"
            except:
                print('{file_path} is not a valid image')
                continue

            new_img = resize_image(im, max_size)
            os.remove(file_path)
            new_img.save(f+'.jpg', 'JPEG', quality=90)


