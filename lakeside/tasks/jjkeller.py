from html import escape
import dateutil
import datetime
import requests
import pandas as pd
from dateutil import tz
import pytz
import traceback


from lakeside.connection.sql_server import LakesideConnections
from lakeside.log import create_log


class JJKeller:
    def __init__(self) -> None:
        self.lc = LakesideConnections()

        self.__set_credentials()
        self.token = None
        self.token_expiration = None
        self.company_root_id = None

    def __set_credentials(self):
        """Sets JJKeller user credentials"""
        self.user_name, self.password = self.lc.get_jjkeller_creds()
        self.payload = f"UserName={self.user_name}&Password={self.password}"

    def __get_token(self):
        """Returns API Token. If there is an existing token and it is expired,
        or near expiration it will recreate the token. If the token does not exist it will created it."""

        if self.token is None:
            url = "https://www.kellerencompass.com/API/Token/Create"

            headers = {
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept": "application/json",
            }

            response = dict(
                requests.request("POST", url, headers=headers, data=self.payload).json()
            )

            self.token = response.get("Token")
            self.token_expiration = dateutil.parser.parse(
                response.get("ExpirationDate")
            ).replace(tzinfo=None)
            return self.token
        elif (self.token_expiration - datetime.datetime.utcnow()).total_seconds() > 60:
            return self.token
        elif (self.token_expiration - datetime.datetime.utcnow()).total_seconds() > 0:
            self.__destroy_token()
            return self.__get_token()
        else:
            self.token = None
            return self.__get_token()

    def __destroy_token(self):
        if self.token is None:
            return

        url = "https://www.kellerencompass.com/API/token/destroy"
        payload = {}
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self.token}",
            "Accept": "application/json",
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        if response.text() == "1":
            self.token = None
        else:
            raise f"Error Destroying Token: {response.text()}"

    def __get_company_root_id(self):
        if self.company_root_id is None:
            url = "https://www.kellerencompass.com/API/company/getCGRootList"

            token = self.__get_token()

            payload = {}
            headers = {
                "Content-Type": "application/json",
                "Authorization": f"Bearer {token}",
                "Accept": "application/json",
            }

            response = requests.request(
                "GET", url, headers=headers, data=payload
            ).json()[0]

            cg_root_id = response.get("CGRootId")

            self.company_root_id = cg_root_id
        return self.company_root_id

    def __get_unit_details(self, url: str):
        token = self.__get_token()
        payload = {}
        headers = {
            "Authorization": f"Bearer {token}",
            "Content-Type": "application/json",
            "Accept": "application/json",
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        return response.json()

    def __convert_utc_pt_str(self, datetime, time_zone=None) -> str:
        """Converts utc time to local time and returns it as a string

        Keyword Arguments:
        datetime - datetime object to be converted
        time_zone - timezone for date to be converted to (Default None is US/Pacific)
        """

        if pd.isnull(datetime):
            return None

        datetime = pytz.utc.localize(dateutil.parser.parse(datetime))

        if time_zone is None:
            time_zone = tz.gettz("US/Pacific")

        conv_dt = datetime.astimezone(time_zone)
        return str(conv_dt)[:19]

    def __df_to_sql(self, df: pd.DataFrame, name: str, data_types=None):
        engine = self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage")
        self.lc.df_to_sql(
            engine,
            "JJKeller",
            df,
            name,
            data_types,
        )

    def __exec_command(self, command: str, values: list = None):
        self.lc.execute_command(self.lc.source_data(), command=command, values=values)

    def __date_str(self, date):
        if date is None:
            return date
        return date[:10]

    def get_units(self):
        token = self.__get_token()

        url = f"https://www.kellerencompass.com/API/Unit/GetList/{self.__get_company_root_id()}"

        payload = {}
        headers = {
            "Authorization": f"Bearer {token}",
            "Content-Type": "application/json",
            "Accept": "application/json",
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        units = []
        for unit in response.json():
            for link in unit["Links"]:
                if link["Rel"] == "self":
                    units.append(self.__get_unit_details(link["Href"]))

        return units

    def units_to_sql(self, units: list = None):

        log_id = create_log(self.lc.source_data(), task="JJ Keller Units")

        try:

            if units is None:
                units = self.get_units()

            units_df = pd.DataFrame(units)

            date_cols = [
                "InServiceDate",
                "DispositionDate",
                "PlateExpirationDate",
                "TagDate",
                "LeaseDate",
                "DateLastOdometerRead",
                "DateHourMeterRead",
                "PurchaseDate",
            ]

            for col in date_cols:
                units_df[col] = units_df[col].apply(self.__date_str)

            include_cols = [
                "UnitId",
                "CompanyId",
                "CompanyLevelId",
                "UnitCode",
                "Description",
                "Location",
                "VIN",
                "SecondaryVIN",
                "UnitType",
                "UnitMake",
                "Model",
                "ModelYear",
                "AcquisitionDate",
                "InServiceDate",
                "DispositionDate",
                "DispositionReason",
                "OperationStatus",
                "Weight",
                "FuelType",
                "AppropriationTag",
                "TowedUnitType",
                "LicenseState",
                "LicensePlate",
                "IsCommercialMotorVehicle",
                "HasSeatBelts",
                "PoweredUnitCabType",
                "OwnerType",
                "OwnerName",
                "OwnerContactInfo",
                "Builder",
                "PurchasePrice",
                "WeightGross",
                "WeightCombinedGross",
                "DistanceEnum",
                "DepreciationCost",
                "LicenseFees",
                "InsuranceCost",
                "RentalOrLeaseFees",
                "OtherCost",
                "IsLiftAxleEquipped",
                "IsTwoAxlePullTrailer",
                "IsTwoAxleWith5thWheel",
                "IsIntraStateUnit",
                "IsReeferEquipped",
                "IsPtoEquipped",
                "CanHaulLogs",
                "NumberOfSeats",
                "DefaultMPG",
                "RecordHours",
                "Capacity",
                "TitleState",
                "TitleNumber",
                "County",
                "PlateExpirationDate",
                "TagDate",
                "Fleet",
                "LeaseDate",
                "TitledOwner",
                "LienHolder",
                "UnladenWeight",
                "PermittingWeight",
                "NumberOfAxles",
                "IsGliderKitEquipped",
                "NumberOfCylinders",
                "FuelTaxJurisdiction",
                "MinimumMPG",
                "MaximumMPG",
                "FactoryPrice",
                "PurchaseDate",
                "IsHouseholdGoods",
                "BeginningOdometer",
                "CurrentOdometer",
                "DateLastOdometerRead",
                "BeginningHourMeter",
                "CurrentHourMeter",
                "DateHourMeterRead",
                "FieldLabel1",
                "FieldValue1",
                "FieldLabel2",
                "FieldValue2",
                "FieldLabel3",
                "FieldValue3",
                "FieldLabel4",
                "FieldValue4",
                "FieldLabel5",
                "FieldValue5",
                "FieldLabel6",
                "FieldValue6",
                "FieldLabel7",
                "FieldValue7",
                "FieldLabel8",
                "FieldValue8",
                "FieldLabel9",
                "FieldValue9",
                "FieldLabel10",
                "FieldValue10",
            ]

            self.__df_to_sql(units_df[include_cols], "Units")

            self.__exec_command("{ CALL JJKeller.pMoveUnits }")

            create_log(self.lc.source_data(), id=log_id, status="success")

        except Exception as e:
            print(str(e))
            create_log(
                self.lc.source_data(), id=log_id, status="failure", message=str(e)
            )

    def unit_locations_to_sql(self, start_date: str = None):

        log_id = create_log(self.lc.source_data(), task='JJ Keller Update Locations')

        try:

            if start_date is None or start_date.upper() == "YESTERDAY":
                start_date = str(datetime.date.today() - datetime.timedelta(days=1))
            elif start_date.upper() == 'TODAY':
                start_date = str(datetime.date.today())

            url = f"https://www.kellerencompass.com/API/Unit/GetGPSForCGRoot?id={self.__get_company_root_id()}&startDate={start_date}&endDate="

            payload = {}
            headers = {
                "Authorization": f"Bearer {self.__get_token()}",
                "Content-Type": "application/json",
                "Accept": "application/json",
            }

            response = requests.request("GET", url, headers=headers, data=payload)

            loc_df = pd.DataFrame(response.json())

            if loc_df.shape[0] != 0:
                include_columns = ["UnitId", "DateTime", "Latitude", "Longitude"]

                for col in include_columns:
                    if col not in loc_df.columns:
                        loc_df[col] = None

                max_date_df = loc_df[
                    loc_df.groupby("UnitId").UTCTime.transform("max") == loc_df["UTCTime"]
                ]

                max_date_df["DateTime"] = max_date_df["UTCTime"].apply(
                    self.__convert_utc_pt_str
                )

                self.__df_to_sql(max_date_df[include_columns], "UnitLocations")

                self.__exec_command("{ CALL JJKeller.pUpdateLocations }")

            create_log(self.lc.source_data(), id = log_id, status = 'success')

        except Exception as e:
            traceback.print_exc()
            create_log(self.lc.source_data(), id=log_id, status='failure', message=traceback.format_exc()+'\n\n'+str(response.json()))


