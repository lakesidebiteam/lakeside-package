import pandas as pd
from requests.models import Response
from requests.sessions import session
import xmltodict
from requests_oauthlib import OAuth1Session, OAuth2Session
from pandas import DataFrame
import traceback

from lakeside.connection.sql_server import LakesideConnections
from lakeside.tools.dates import convert_str_utc_pt_str
from lakeside.log import create_log


class JohnDeere:
    def __init__(self) -> None:
        self.lc = LakesideConnections()

        self.session = None

    def __create_oauth2_session(self):
        con = self.lc.source_data()
        cursor = con.cursor()
        cursor.execute("SELECT * FROM JohnDeere.OAuth2Credentials")
        data = cursor.fetchone()

        scopes = {"eq1", "eq2"}
        client_id = data[0]
        redirect_uri = data[3]
        client_secret = data[1]
        token_url = data[2]
        refresh_token = data[4]

        self.session = OAuth2Session(
            client_id=client_id, redirect_uri=redirect_uri, scope=scopes
        )
        self.session.refresh_token(
            token_url, refresh_token=refresh_token, auth=(client_id, client_secret)
        )

    def oauth_get(self, url):
        if self.session is None:
            self.__create_oauth2_session()

        headers = {
            "Content-Type": "application/vnd.deere.axiom.v3+json",
            "Accept": "application/vnd.deere.axiom.v3+json",
        }

        response = self.session.get(url, headers=headers)
        return response.json()

    def iterate_pages(self, url):
        data = self.oauth_get(url)

        values = data["values"]

        next_page = ""
        for link in data.get("links", []):
            if link["rel"] == "nextPage":
                next_page = link["uri"]

        if next_page != "":
            values.extend(self.iterate_pages(next_page))

        return values

    def __get_aemp_credentials(self):
        con = self.lc.source_data()
        cursor = con.cursor()

        command = "SELECT * FROM JohnDeere.AEMPCredentials"
        cursor.execute(command)

        data = cursor.fetchone()

        self.aemp_creds = {
            "app_id": data[0],
            "app_secret": data[1],
            "owner_key": data[2],
            "owner_secret": data[3],
        }

    def __df_to_sql(self, df: DataFrame, name: str, data_types=None):
        engine = self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage")
        self.lc.df_to_sql(
            engine,
            "JohnDeere",
            df,
            name,
            data_types,
        )

    def __exec_command(self, command: str, values: list = None):
        self.lc.execute_command(self.lc.source_data(), command=command, values=values)

    def get_equipment_ids(self):
        """Returns a list of all the John Deere Equipment IDs"""
        con = self.lc.source_data()
        cursor = con.cursor()
        cursor.execute("SELECT EquipmentID FROM JohnDeere.Equipment")

        data = [i[0] for i in cursor.fetchall()]

        return data

    def aemp_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="John Deere AEMP")
        print(f"Starting process with Log ID: {log_id}")
        try:
            print("Getting credentials")
            self.__get_aemp_credentials()

            oauth = OAuth1Session(
                self.aemp_creds["app_id"],
                client_secret=self.aemp_creds["app_secret"],
                resource_owner_key=self.aemp_creds["owner_key"],
                resource_owner_secret=self.aemp_creds["owner_secret"],
            )

            url = "https://sandboxapi.deere.com/aemp/Fleet/1"

            print("Getting data from API")
            response = oauth.get(url)
            print(response.reason)

            print("Processing data")
            equip_dict = xmltodict.parse(response.content)

            equipment = equip_dict["Fleet"]["Equipment"]

            equip_list = []

            for eq in equipment:
                equip_list.append(
                    {
                        "oem_name": eq["EquipmentHeader"].get("OEMName"),
                        "model": eq["EquipmentHeader"].get("Model"),
                        "equipment_id": eq["EquipmentHeader"].get("EquipmentID"),
                        "serial_number": eq["EquipmentHeader"].get("SerialNumber"),
                        "pin": eq["EquipmentHeader"].get("PIN"),
                        "location_datetime": convert_str_utc_pt_str(
                            eq["Location"].get("@datetime")
                        ),
                        "latitude": eq["Location"].get("Latitude"),
                        "longitude": eq["Location"].get("Longitude"),
                        "altitude": eq["Location"].get("Altitude"),
                        "altitude_units": eq["Location"].get("AltitudeUnits"),
                        "idle_hrs_datetime": convert_str_utc_pt_str(
                            eq.get("CumulativeIdleHours", {}).get("@datetime")
                        ),
                        "idle_hrs": eq.get("CumulativeIdleHours", {}).get("Hour"),
                        "operating_hrs_datetime": convert_str_utc_pt_str(
                            eq.get("CumulativeIdleHours", {}).get("@datetime")
                        ),
                        "operating_hrs": eq["CumulativeOperatingHours"].get("Hour"),
                        "operating_hrs_datetime": convert_str_utc_pt_str(
                            eq.get("CumulativeIdleHours", {}).get("@datetime")
                        ),
                        "operating_hrs": eq["CumulativeOperatingHours"].get("Hour"),
                        "def_datetime": eq.get("DEFRemaining", {}).get("@datetime"),
                        "def_percent": eq.get("DEFRemaining", {}).get("Percent"),
                        "fuel_used_datetime": convert_str_utc_pt_str(
                            eq.get("FuelUsed", {}).get("@datetime")
                        ),
                        "fuel_used_units": eq.get("FuelUsed", {}).get("FuelUnits"),
                        "fuel_used": eq.get("FuelUsed", {}).get("FuelConsumed"),
                        "fuel_remaining_datetime": convert_str_utc_pt_str(
                            eq.get("FuelRemaining", {}).get("@datetime")
                        ),
                        "fuel_remaining_percent": eq.get("FuelRemaining", {}).get(
                            "Percent"
                        ),
                    }
                )

            print("Staging data in SQL")
            equip_df = pd.DataFrame(equip_list)

            self.__df_to_sql(equip_df, "AEMP")
            print("Transferring data to SourceData and update equipment locations")
            self.lc.execute_command(
                self.lc.source_data(), "{ CALL JohnDeere.pMoveAEMP }"
            )
            create_log(self.lc.source_data(), id=log_id, status="success")
            print("Process completed")
        except:
            traceback.print_exc()
            create_log(
                self.lc.source_data(), id=log_id, status="failure", message=traceback.format_exc()
            )

    def equipment_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="John Deere Equipment")

        try:

            url = "https://sandboxapi.deere.com/platform/organizations/1880/machines;count=100"

            response = self.iterate_pages(url)

            for i, ma in enumerate(response):
                response[i]["category_id"] = ma.get("category", {}).get("id")
                response[i]["category"] = ma.get("category", {}).get("name")

                response[i]["detailMachineCode"] = ma.get("detailMachineCode", {}).get(
                    "name"
                )

                response[i]["equipmentMake"] = ma.get("equipmentMake", {}).get("name")

                response[i]["equipmentModel"] = ma.get("equipmentModel", {}).get("name")

                response[i]["equipmentType"] = ma.get("equipmentType", {}).get("name")

                response[i]["equipmentApexType"] = ma.get("equipmentApexType", {}).get(
                    "name"
                )
                response[i].pop("links")
                response[i].pop("machineCategories")
                response[i].pop("make")
                response[i].pop("model")
                response[i].pop("terminals")
                response[i].pop("@type")
                response[i].pop("capabilities")
                response[i].pop("displays")

            df = pd.DataFrame(response)

            self.__df_to_sql(df, "Equipment")

            self.__exec_command("{ CALL JohnDeere.pMoveEquipment }")

            create_log(
                self.lc.source_data(),
                id=log_id,
                status="success",
                message=traceback.format_exc(),
            )

        except:
            traceback.print_exc()
            create_log(
                self.lc.source_data(),
                id=log_id,
                status="failure",
                message=traceback.format_exc(),
            )

    def engine_hours_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="John Deere Engine Hours")

        try:
            con = self.lc.source_data()
            cursor = con.cursor()

            command = """Select

                            A.EquipmentID

                            ,CONCAT(LEFT(CONVERT(varchar(255), ISNULL(
								CASE
									WHEN DATEADD(day, -14, GETDATE()) > MAX(B.ReportTime)
									THEN DATEADD(day, -14, GETDATE())
									ELSE MAX(B.ReportTime)
								END, '20200101'), 126), 19),'.000Z')  as LatestReportTime

                            ,CONCAT(CONVERT(varchar(255), GETUTCDATE(), 126),'Z') as NowUTC

                        FROM JohnDeere.Equipment as A
                        LEFT JOIN JohnDeere.EngineHours as B
                            ON A.EquipmentID = B.EquipmentID

                        Group by A.EquipmentID"""

            cursor.execute(command)

            data = cursor.fetchall()

            for equip in data:
                # print(f'Getting data for: {equip[0]}')

                url = f'https://sandboxapi.deere.com/platform/machines/{equip[0]}/engineHours;count=100?startDate={equip[1]}&endDate={equip[2]}'

                values = self.iterate_pages(url)
                if equip[0] != 384839:
                    for i, value in enumerate(values):
                        values[i]['equipment'] = equip[0]
                        values[i].pop('@type')
                        values[i].pop('links')
                        values[i]['reading_unit'] = value.get('reading', {}).get('unit')
                        values[i]['reading'] = value.get('reading', {}).get('valueAsDouble')
                        values[i]['source'] = value.get('source', '')
                        values[i]['reportTime'] = convert_str_utc_pt_str(value.get('reportTime'))

                    df = pd.DataFrame(values)
                    # print('Loading to SQL')

                    self.__df_to_sql(df, 'EngineHours')
                    # print('Transferring data')
                    self.__exec_command('{ CALL JohnDeere.pMoveEngineHours }')

            create_log(self.lc.source_data(), id=log_id, status='success')

        except:
            traceback.print_exc()
            create_log(self.lc.source_data(), id=log_id, status='failure', message=traceback.format_exc())

    def hours_of_operation_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="John Deere Hours of Operation")

        try:
            con = self.lc.source_data()
            cursor = con.cursor()

            command = """Select

                            A.EquipmentID

                            ,CONCAT(LEFT(CONVERT(varchar(255), ISNULL(
								CASE
									WHEN DATEADD(day, -14, GETDATE()) > MAX(B.StartDate)
									THEN DATEADD(day, -14, GETDATE())
									ELSE MAX(B.StartDate)
								END, '20200101'), 126), 19),'.000Z')  as LatestReportTime

                            ,CONCAT(CONVERT(varchar(255), GETUTCDATE(), 126),'Z') as NowUTC

                        FROM JohnDeere.Equipment as A
                        LEFT JOIN JohnDeere.HoursOfOperation as B
                        ON A.EquipmentID = B.EquipmentID

                        Group by A.EquipmentID
                        ORDER BY A.EquipmentID"""

            cursor.execute(command)

            data = cursor.fetchall()

            for equip in data:
                print(f'Starting Equipment ID: {equip[0]}')
                url = f'https://sandboxapi.deere.com/platform/machines/{equip[0]}/hoursOfOperation;count=100?startDate={equip[1]}&endDate={equip[2]}'

                data = self.iterate_pages(url)
                if len(data) == 0:
                    continue
                df = pd.DataFrame(data)

                print(f'Returned {df.shape[0]} records')    

                df['endDate'] = df['endDate'].apply(convert_str_utc_pt_str)
                df['startDate'] = df['startDate'].apply(convert_str_utc_pt_str)
                df['equipment_id'] = equip[0]

                print('Transferring to SQL')

                self.__df_to_sql(df[['equipment_id', 'startDate', 'endDate', 'engineState']],
                    'HoursOfOperation')
                
                print('Moving data')

                self.__exec_command('{ CALL JohnDeere.pMoveHoursOfOperation }')

            create_log(self.lc.source_data(), id=log_id, status='success')

        except:
            traceback.print_exc()
            create_log(self.lc.source_data(), id=log_id, status='failure', message=traceback.format_exc())

    def location_history_to_sql(self):

        log_id = create_log(self.lc.source_data(), task="John Deere Location History")

        try:
            con = self.lc.source_data()
            cursor = con.cursor()

            command = """Select

                            A.EquipmentID

                            ,CONCAT(LEFT(CONVERT(varchar(255), ISNULL(
                                CASE
                                    WHEN DATEADD(day, -14, GETDATE()) > MAX(B.EventTimestamp)
                                    THEN DATEADD(day, -14, GETDATE())
                                    ELSE MAX(B.EventTimestamp)
                                END, DATEADD(day, -28, GETDATE())), 126), 19),'.000Z')  as LatestReportTime

                            ,CONCAT(CONVERT(varchar(255), GETUTCDATE(), 126),'Z') as NowUTC

                        FROM JohnDeere.Equipment as A
                        LEFT JOIN JohnDeere.LocationHistory as B
                            ON A.EquipmentID = B.EquipmentID

                        Group by A.EquipmentID"""

            cursor.execute(command)

            data = cursor.fetchall()

            for equip in data:
                print(f'Starting Equipment ID: {equip[0]}' )
                url = f'https://sandboxapi.deere.com/platform/machines/{equip[0]}/locationHistory;count=100?startDate={equip[1]}&endDate={equip[2]}'

                data = self.iterate_pages(url)

                if len(data) == 0:
                    continue

                for i, row in enumerate(data):
                    data[i]['latitude'] = row.get('point', {}).get('lat')
                    data[i]['longitude'] = row.get('point', {}).get('lon')
                    data[i].pop('@type')
                    data[i].pop('point')
                    data[i].pop('links')

                
                df = pd.DataFrame(data)

                print(f'Returned {df.shape[0]} records')    

                df['eventTimestamp'] = df['eventTimestamp'].apply(convert_str_utc_pt_str)
                df['gpsFixTimestamp'] = df['gpsFixTimestamp'].apply(convert_str_utc_pt_str)
                df['equipment_id'] = equip[0]

                print('Transferring to SQL')

                self.__df_to_sql(df, 'LocationHistory')
                
                print('Moving data')

                self.__exec_command('{ CALL JohnDeere.pMoveLocationHistory }')

            create_log(self.lc.source_data(), id=log_id, status='success')

        except:
            traceback.print_exc()
            create_log(self.lc.source_data(), id=log_id, status='failure', message=traceback.format_exc())