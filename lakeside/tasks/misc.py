import requests
from bs4 import BeautifulSoup
from pprint import pprint
import pandas as pd
from lakeside.connection.sql_server import LakesideConnections
from lakeside.log import create_log
import traceback


def asphalt_binder_to_sql():
    """Scrapes Asphalt Binder Reference Cost from WSDOT website and stores in SourceData"""
    lc = LakesideConnections()

    log_id = create_log(lc.source_data(), task="Update WSDOT Apshalt Binder")
    
    try:

        response = requests.get('https://wsdot.wa.gov/Business/Construction/AsphaltBinderReferenceCost.htm')

        soup = BeautifulSoup(response.text, 'html.parser')

        table = soup.find('table', attrs={'class': "tablesorter"})
        table_body = table.find('tbody')

        data = []
        rows = table_body.find_all('tr')
        for row in rows:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            data.append([ele for ele in cols])

        col_names = ['DateEffective', 'BeginPeriod', 'EndPeriod', 'Eastern', 'Western']
        df = pd.DataFrame(data, columns=col_names)

        lc.df_to_sql(
            lc.create_pandas_connection('DWSQLSRV', 'SourceData_Stage'),
            'dbo',
            df,
            'AsphaltBinder',
        )

        lc.execute_command(lc.source_data(), '{ CALL pMoveAsphaltBinder }')
        create_log(lc.source_data(), id=log_id, status='success')

    except:
        traceback.print_exc()
        create_log(lc.source_data(), id=log_id, status='failure', message=traceback.format_exc())