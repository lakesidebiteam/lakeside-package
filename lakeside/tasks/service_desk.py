from datetime import datetime
import pandas as pd
import phonenumbers as pn
import re
import requests
from sqlalchemy import create_engine, event, types, String
import traceback

from lakeside.connection.sql_server import LakesideConnections
from lakeside.log import create_log

class ServiceDesk:
    """Reload data from ServiceDesk into SourceData"""
    def __init__(self) -> None:

        self.lc = LakesideConnections()
        self.print_status = True

    def __create_con(self):
        return self.lc.create_sql_connection("SourceData")

    def __create_pd_con(self):
        return self.lc.create_pandas_connection("DWSQLSRV", "SourceData_Stage")

    def __get_credentials(self):
        connection = self.__create_con()
        credentials = {}
        cursor = connection.cursor()
        cursor.execute("Select Top 1 * From ServiceDesk.API")
        results = cursor.fetchone()
        credentials = results[0]
        return credentials

    def __get_headers(self):
        token = self.__get_credentials()

        headers = {
            "X-Samanage-Authorization": "Bearer " + token,
            "Accept": "application/vnd.samanage.v2.1+json",
        }

        return headers

    def __prints(self, text):
        if self.print_status:
            print(text)

    def update_categories(self, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Categories")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Categories")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:

            data = []

            link = "https://api.samanage.com/categories.json?per_page=100"

            page_number = 1

            while True:
                self.__prints(f"Loading Page {page_number}")
                response = requests.get(link, headers=self.__get_headers())

                data.extend(response.json())

                if "next" in response.links.keys():
                    link = response.links["next"]["url"]
                    page_number += 1
                else:
                    break

            category = {
                "id": [],
                "name": [],
                "default_assignee_id": [],
                "default_tags": [],
                "parent_id": [],
            }

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            for i in data:
                category["id"].append(i["id"])
                category["name"].append(i["name"])
                category["default_assignee_id"].append(i["default_assignee_id"])
                category["default_tags"].append(i["default_tags"])
                category["parent_id"].append(i["parent_id"])

            categories_df = pd.DataFrame(category)

            self.__prints("Copying data to SQL")
            # Copy DF to SQL
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                categories_df,
                "Categories",
                {col_name: types.VARCHAR(length=255) for col_name in categories_df},
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveCategories }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Categories Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))

    def update_users(self, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Users")
        self.__prints("")

        log_id = create_log(self.__create_con(), task="ServiceDesk Users")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:

            data = []

            link = "https://api.samanage.com/users.json?per_page=100"

            page_number = 1

            while True:
                self.__prints(f"Loading Page {page_number}")
                response = requests.get(link, headers=self.__get_headers())

                data.extend(response.json())

                if "next" in response.links.keys():
                    link = response.links["next"]["url"]
                    page_number += 1
                else:
                    break

            user_data = {
                "id": [],
                "created": [],
                "updated": [],
                "last_login": [],
                "mfa_enabled": [],
                "phone": [],
                "mobile": [],
                "email": [],
                "name": [],
                "disabled": [],
                "avatar_url": [],
                "role_id": [],
                "title": [],
                "reports_to_user_id": [],
                "reports_to_group_id": [],
            }

            user_groups = {"user_id": [], "group_id": []}

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            print(data)

            for i in data:
                user_data["id"].append(i["id"])
                user_data["created"].append(i["created_at"])
                user_data["updated"].append(i["updated_at"])
                user_data["last_login"].append(i["last_login"])
                if "mfa_enabled" in i.keys():
                    user_data["mfa_enabled"].append(i["mfa_enabled"])
                else:
                    user_data["mfa_enabled"].append(None)

                # Phones
                try:
                    phone = pn.format_number(
                        pn.parse(i["phone"], "US"), pn.PhoneNumberFormat.NATIONAL
                    )
                except:
                    phone = i["phone"]
                try:
                    mobile = pn.format_number(
                        pn.parse(i["mobile_phone"], "US"), pn.PhoneNumberFormat.NATIONAL
                    )
                except:
                    mobile = i["mobile_phone"]

                user_data["phone"].append(phone)
                user_data["mobile"].append(mobile)

                user_data["email"].append(i["email"])
                user_data["name"].append(i["name"])
                user_data["disabled"].append(i["disabled"])
                if i["avatar"]["type"] == "image":
                    user_data["avatar_url"].append(i["avatar"]["avatar_url"])
                else:
                    user_data["avatar_url"].append(None)
                user_data["role_id"].append(i["role"]["id"])
                user_data["title"].append(i["title"])
                if i.get("reports_to") is None:
                    user_data["reports_to_user_id"].append(None)
                    user_data["reports_to_group_id"].append(None)
                else:
                    user_data["reports_to_user_id"].append(i["reports_to"]["id"])
                    user_data["reports_to_group_id"].append(i["reports_to"]["group_id"])

                for j in i["group_ids"]:
                    user_groups["user_id"].append(i["id"])
                    user_groups["group_id"].append(j)

            user_df = pd.DataFrame(user_data)
            group_df = pd.DataFrame(user_groups)

            self.__prints("Copying data to SQL")

            # Copy DataFrame to SQL Server
            data_type = {col_name: types.VARCHAR(length=255) for col_name in user_df}
            data_type["avatar_url"] = types.VARCHAR(length=1000)
            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", user_df, "Users", data_type,
            )

            data_type = {col_name: types.VARCHAR(length=255) for col_name in group_df}
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                group_df,
                "UserGroups",
                data_type,
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveUsers }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Users Completed")

        except Exception as e:
            self.__prints(traceback.print_exc())
            create_log(self.__create_con(), log_id, status="failure", message=traceback.format_exc())

    def update_software(self, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Software")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Software")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:
            data = []

            link = "https://api.samanage.com/softwares.json?per_page=100"

            page_number = 1

            while True:
                self.__prints(f"Loading Page {page_number}")

                response = requests.get(link, headers=self.__get_headers())

                data.extend(response.json())

                page_number += 1

                if (
                    "next" in response.links.keys()
                    and response.links["next"]["url"] != link
                ):
                    link = response.links["next"]["url"]
                else:
                    break

            software = {
                "id": [],
                "name": [],
                "category_id": [],
                "category_name": [],
                "created_at": [],
                "custom": [],
                "first_detected": [],
                "greynet": [],
                "hidden": [],
                "tag": [],
                "updated_at": [],
                "vendor_id": [],
                "vendor_name": [],
            }

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            for i in data:
                software["id"].append(i["id"])
                software["name"].append(i["name"])
                if i["category"] is None:
                    software["category_id"].append(None)
                    software["category_name"].append(None)
                else:
                    software["category_id"].append(i["category"]["id"])
                    software["category_name"].append(i["category"]["name"])
                software["created_at"].append(i["created_at"])
                software["custom"].append(i["custom"])
                software["first_detected"].append(i["first_detected"])
                software["greynet"].append(i["greynet"])
                software["hidden"].append(i["hidden"])
                software["tag"].append(i["tag"])
                software["updated_at"].append(i["updated_at"])
                software["vendor_id"].append(i["vendor"]["id"])
                software["vendor_name"].append(i["vendor"]["name"])

            software_df = pd.DataFrame(software)

            self.__prints("Copying data to SQL")
            # Copy DF to SQL
            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in software_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                software_df,
                "Software",
                data_type,
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveSoftware }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Sofware Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))

    def update_sites(self, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Sites")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Sites")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:

            data = []

            link = "https://api.samanage.com/sites.json?per_page=100&layout=long"

            page_number = 1

            while True:
                self.__prints(f"Loading Page {page_number}")

                response = requests.get(link, headers=self.__get_headers())

                data.extend(response.json())

                if "next" in response.links.keys():
                    link = response.links["next"]["url"]
                    page_number += 1
                else:
                    break

            self.__prints("Loading Data Completed.")
            self.__prints("{} records loaded.".format(str(len(data))))

            sites = {"id": [], "name": [], "location": [], "description": []}

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            for i in data:
                sites["id"].append(i["id"])
                sites["name"].append(i["name"])
                sites["location"].append(i["location"])
                sites["description"].append(i["description"])

            df = pd.DataFrame(sites)

            self.__prints("Copying data to SQL")
            # Copy DF to SQL
            data_type = {col_name: types.VARCHAR(length=255) for col_name in df}
            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", df, "Sites", data_type,
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveSites }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Sites Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))

    def update_problems(self, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Problems")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Problems")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:

            data = []

            link = "https://api.samanage.com/problems.json?per_page=100&layout=long"

            page_number = 1

            while True:
                self.__prints(f"Loading Page {page_number}")

                response = requests.get(link, headers=self.__get_headers())

                data.extend(response.json())

                if "next" in response.links.keys():
                    link = response.links["next"]["url"]
                    page_number += 1
                else:
                    break

            self.__prints("Loading Data Completed.")
            self.__prints("{} records loaded.".format(str(len(data))))

            problems = {
                "id": [],
                "assignee_id": [],
                "created_at": [],
                "created_by_id": [],
                "department_id": [],
                "description": [],
                "description_no_html": [],
                "href": [],
                "name": [],
                "number": [],
                "priority": [],
                "requester_id": [],
                "root_cause": [],
                "site_id": [],
                "state": [],
                "symptoms": [],
                "total_time_spent": [],
                "updated_at": [],
                "workaround": [],
            }

            attachments = {
                "problem_id": [],
                "filename": [],
                "secure_url": [],
            }

            comments = {
                "problem_id": [],
                "comment_id": [],
                "body": [],
                "created_at": [],
                "is_task": [],
                "is_private": [],
                "updated_at": [],
                "user_id": [],
            }

            incidents = {
                "problem_id": [],
                "incident_id": [],
            }

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            for i in data:
                problems["id"].append(i["id"])
                if i["assignee"] is None:
                    problems["assignee_id"].append(None)
                elif not i["assignee"]["is_user"]:
                    problems["assignee_id"].append(None)
                else:
                    problems["assignee_id"].append(i["assignee"]["id"])
                problems["created_at"].append(i["created_at"])
                if i["created_by"] is None:
                    problems["created_by_id"].append(None)
                else:
                    problems["created_by_id"].append(i["created_by"]["user_id"])
                if i["department"] is None:
                    problems["department_id"].append(None)
                else:
                    problems["department_id"].append(i["department"]["id"])
                problems["description"].append(i["description"][0:8000])
                problems["description_no_html"].append(i["description_no_html"][0:8000])
                problems["href"].append(i["href"])
                problems["name"].append(i["name"])
                problems["number"].append(i["number"])
                problems["priority"].append(i["priority"])
                if i["requester"] is None:
                    problems["requester_id"].append(None)
                else:
                    problems["requester_id"].append(i["requester"]["user_id"])
                problems["root_cause"].append(i["root_cause"])
                if i["site"] is None:
                    problems["site_id"].append(None)
                else:
                    problems["site_id"].append(i["site"]["id"])
                problems["state"].append(i["state"])
                problems["symptoms"].append(i["symptoms"])
                problems["total_time_spent"].append(i["total_time_spent"])
                problems["updated_at"].append(i["updated_at"])
                problems["workaround"].append(i["workaround"])

                for j in i["attachments"]:
                    attachments["problem_id"].append(i["id"])
                    attachments["filename"].append(j["filename"])
                    attachments["secure_url"].append(j["secure_url"])

                for j in i["comments"]:
                    comments["problem_id"].append(i["id"])
                    comments["comment_id"].append(j["id"])
                    comments["body"].append(j["body"])
                    comments["created_at"].append(j["created_at"])
                    comments["is_task"].append(j["isTask"])
                    comments["is_private"].append(j["is_private"])
                    comments["updated_at"].append(j["updated_at"])
                    comments["user_id"].append(j["user"]["id"])

                for j in i["incidents"]:
                    incidents["problem_id"].append(i["id"])
                    incidents["incident_id"].append(j["id"])

            problem_df = pd.DataFrame(problems)
            attachments_df = pd.DataFrame(attachments)
            comments_df = pd.DataFrame(comments)
            incidents_df = pd.DataFrame(incidents)

            self.__prints("Copying data to SQL")
            # Copy DF to SQL
            data_type = {col_name: types.VARCHAR(length=255) for col_name in problem_df}
            data_type["description"] = types.VARCHAR(length=8000)
            data_type["description_no_html"] = types.VARCHAR(length=8000)
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                problem_df,
                "Problems",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in attachments_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                attachments_df,
                "ProblemsAttachments",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in comments_df
            }
            data_type["body"] = types.VARCHAR(length=8000)
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                comments_df,
                "ProblemsComments",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in incidents_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                incidents_df,
                "ProblemsIncidents",
                data_type,
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveProblems }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Problems Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))

    def update_incidents(self, today_only=False, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Incidents" + " Today" if today_only else "")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Incidents")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:
            data = []

            if today_only:
                link = "https://api.samanage.com/incidents.json?layout=long&page={}&per_page=100&updated[]=1"
            else:
                link = "https://api.samanage.com/incidents.json?layout=long&page={}&per_page=100"

            page_number = 1
            total_pages = None
            load_errors = 0

            while True:

                try:
                    if load_errors > 3:
                        break

                    response = requests.get(
                        link.format(str(page_number)), headers=self.__get_headers()
                    )

                    total_pages = int(response.headers["X-Total-Pages"])

                    self.__prints(
                        "Incidents Page {} of {} Loaded.".format(
                            str(page_number), str(total_pages)
                        )
                    )

                    data.extend(response.json())

                    if page_number < total_pages:
                        page_number += 1
                    else:
                        break
                except Exception as e:
                    load_errors += 1
                    message = str(e)
                    self.__prints(
                        f"Error loading {page_number} of {total_pages}. Message: {message}."
                    )

            self.__prints("Loading Data Completed.")
            self.__prints("{} records loaded.".format(str(len(data))))

            incident = {
                "id": [],
                "assignee_user_id": [],
                "assignee_group_id": [],
                "category_id": [],
                "category_name": [],
                "cc_list": [],
                "created_at": [],
                "created_by_user_id": [],
                "customer_satisfaction_survey_time": [],
                "criticality": [],
                "restart": [],
                "request_for": [],
                "department_id": [],
                "description": [],
                "due_at": [],
                "is_service_request": [],
                "name": [],
                "number": [],
                "number_of_comments": [],
                "priority": [],
                "requester_user_id": [],
                "resolution_code": [],
                "site_id": [],
                "state": [],
                "subcategory_id": [],
                "subcategory_name": [],
                "updated_at": [],
                "user_saw_all_comments": [],
                "customer_satisfaction_response": [],
                "is_customer_satisfied": [],
                "resolution": [],
                "state_changed_bt": [],
                "state_changed_te": [],
                "state_changed_date": [],
                "to_first_response_bt": [],
                "to_first_response_te": [],
                "to_first_response_date": [],
                "to_resolve_bt": [],
                "to_resolve_te": [],
                "to_resolve_date": [],
                "to_close_bt": [],
                "to_close_te": [],
                "to_close_date": [],
                "seq": [],
            }

            cc = {"incident_id": [], "cc": []}

            sub_incidents = {"incident_id": [], "sub_incident_id": []}

            sla_violations = {
                "incident_id": [],
                "sla_id": [],
                "resolved": [],
                "sla_type": [],
                "time_delay": [],
                "time_units": [],
            }

            attachments = {
                "incident_id": [],
                "content_type": [],
                "filename": [],
                "url": [],
            }

            associated_sla_names = {"incident_id": [], "sla_name": []}

            comments = {
                "incident_id": [],
                "comment_id": [],
                "body": [],
                "created_at": [],
                "updated_at": [],
                "is_private": [],
                "user_id": [],
            }

            solutions = {"incident_id": [], "solution_id": []}

            tags = {"incident_id": [], "tag_id": [], "name": []}

            assets = {"incident_id": [], "asset_id": []}

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            for i in data:
                incident["id"].append(i["id"])

                if i["assignee"] is None:
                    incident["assignee_user_id"].append(None)
                elif i["assignee"]["is_user"]:
                    incident["assignee_user_id"].append(i["assignee"]["id"])
                else:
                    incident["assignee_user_id"].append(None)
                if i["assignee"] is None:
                    incident["assignee_group_id"].append(None)
                elif i["assignee"]["is_user"]:
                    incident["assignee_group_id"].append(i["assignee"]["group_id"])
                else:
                    incident["assignee_group_id"].append(i["assignee"]["id"])
                if i["category"] is None:
                    incident["category_id"].append(None)
                    incident["category_name"].append(None)
                else:
                    incident["category_id"].append(i["category"]["id"])
                    incident["category_name"].append(i["category"]["name"])
                incident["cc_list"].append("; ".join(i["cc"]))
                incident["created_at"].append(i["created_at"])
                incident["created_by_user_id"].append(i["created_by"]["user_id"])
                incident["customer_satisfaction_survey_time"].append(
                    i["created_by"]["customer_satisfaction_survey_time"]
                )

                criticality = ""
                restart = ""
                request_for = ""
                sequence = ""

                for j in i["custom_fields_values"]:
                    if j["custom_field_id"] == 92209:
                        criticality = j["value"]
                    elif j["custom_field_id"] == 89603:
                        restart = j["value"]
                    elif j["custom_field_id"] == 93577:
                        request_for = j["value"]
                    elif j["custom_field_id"] == 116918:
                        sequence = j["value"]

                incident["criticality"].append(criticality)
                incident["restart"].append(restart)
                incident["request_for"].append(request_for)
                incident["seq"].append(sequence)
                if i["department"] is None:
                    incident["department_id"].append(None)
                else:
                    incident["department_id"].append(i["department"]["id"])

                description = re.sub(r"\n\s*\n\s*", r"\n\n", i["description_no_html"])

                incident["description"].append(description[:8000])
                incident["due_at"].append(i["due_at"])
                incident["is_service_request"].append(i["is_service_request"])
                incident["name"].append(i["name"])
                incident["number"].append(i["number"])
                incident["number_of_comments"].append(i["number_of_comments"])
                incident["priority"].append(i["priority"])
                incident["requester_user_id"].append(i["requester"]["user_id"])
                if i["site"] is None:
                    incident["site_id"].append(None)
                else:
                    incident["site_id"].append(i["site"]["id"])
                incident["state"].append(i["state"])
                if i["subcategory"] is None:
                    incident["subcategory_id"].append(None)
                    incident["subcategory_name"].append(None)
                else:
                    incident["subcategory_id"].append(i["subcategory"]["id"])
                    incident["subcategory_name"].append(i["subcategory"]["name"])

                incident["updated_at"].append(i["updated_at"])
                incident["customer_satisfaction_response"].append(
                    i["customer_satisfaction_response"]
                )
                incident["is_customer_satisfied"].append(i["is_customer_satisfied"])
                incident["resolution"].append(i["resolution"])
                incident["resolution_code"].append(i["resolution_code"])
                incident["user_saw_all_comments"].append(i["user_saw_all_comments"])

                state_changed_bt = ""
                state_changed_te = ""
                state_changed_date = ""
                to_first_response_bt = ""
                to_first_response_te = ""
                to_first_response_date = ""
                to_resolve_bt = ""
                to_resolve_te = ""
                to_resolve_date = ""
                to_close_bt = ""
                to_close_te = ""
                to_close_date = ""

                for j in i["statistics"]:

                    bt = j["business_time_elapsed"]
                    te = j["time_elapsed"]
                    date = j["value"]

                    if j["statistic_type"] == "State Changed":
                        state_changed_bt = bt
                        state_changed_te = te
                        state_changed_date = date
                    elif j["statistic_type"] == "to_first_response":
                        to_first_response_bt = bt
                        to_first_response_te = te
                        to_first_response_date = date
                    elif j["statistic_type"] == "to_resolve":
                        to_resolve_bt = bt
                        to_resolve_te = te
                        to_resolve_date = date
                    elif j["statistic_type"] == "to_close":
                        to_close_bt = bt
                        to_close_te = te
                        to_close_date = date

                incident["state_changed_bt"].append(state_changed_bt)
                incident["state_changed_te"].append(state_changed_te)
                incident["state_changed_date"].append(state_changed_date)
                incident["to_first_response_bt"].append(to_first_response_bt)
                incident["to_first_response_te"].append(to_first_response_te)
                incident["to_first_response_date"].append(to_first_response_date)
                incident["to_resolve_bt"].append(to_resolve_bt)
                incident["to_resolve_te"].append(to_resolve_te)
                incident["to_resolve_date"].append(to_resolve_date)
                incident["to_close_bt"].append(to_close_bt)
                incident["to_close_te"].append(to_close_te)
                incident["to_close_date"].append(to_close_date)

                # List of cc email addresses
                for j in i["cc"]:
                    cc["incident_id"].append(i["id"])
                    cc["cc"].append(j)

                # List of sub incidents
                for j in i["incidents"]:
                    sub_incidents["incident_id"].append(i["id"])
                    sub_incidents["sub_incident_id"].append(j["id"])

                # List of SLA Violations
                for j in i["sla_violations"]:
                    sla_violations["incident_id"].append(i["id"])
                    sla_violations["sla_id"].append(j["sla_id"])
                    sla_violations["resolved"].append(j["resolved"])
                    sla_violations["sla_type"].append(j["sla_type"])
                    sla_violations["time_delay"].append(j["time_delay"])
                    sla_violations["time_units"].append(j["time_units"])

                # List of Associated SLA Names
                for j in i["associated_sla_names"]:
                    associated_sla_names["incident_id"].append(i["id"])
                    associated_sla_names["sla_name"].append(j)

                # List of Attachments
                for j in i["attachments"]:
                    attachments["incident_id"].append(i["id"])
                    attachments["content_type"].append(j["content_type"])
                    attachments["filename"].append(j["filename"])
                    attachments["url"].append(j["secure_url"])

                # List of Comments
                for j in i["comments"]:
                    comments["incident_id"].append(i["id"]),
                    comments["comment_id"].append(j["id"]),
                    comments["body"].append(re.sub("<.*?>", "", j["body"])[:8000]),
                    comments["created_at"].append(j["created_at"]),
                    comments["updated_at"].append(j["updated_at"]),
                    comments["is_private"].append(j["is_private"]),
                    if j["user"] is None:
                        comments["user_id"].append(None)
                    else:
                        comments["user_id"].append(j["user"]["id"])

                # List of Solutions
                for j in i["solutions"]:
                    solutions["incident_id"].append(i["id"])
                    solutions["solution_id"].append(j["id"])

                for j in i["tags"]:
                    tags["incident_id"].append(i["id"])
                    tags["tag_id"].append(0) #(j["id"])
                    tags["name"].append(j["name"])

                # List of Assets
                for j in i["assets"]:
                    assets["incident_id"].append(i["id"])
                    assets["asset_id"].append(j["id"])

            incident_df = pd.DataFrame(incident)
            cc_df = pd.DataFrame(cc)
            assets_df = pd.DataFrame(assets)
            associated_sla_names_df = pd.DataFrame(associated_sla_names)
            attachments_df = pd.DataFrame(attachments)
            comments_df = pd.DataFrame(comments)
            sla_violations_df = pd.DataFrame(sla_violations)
            solutions_df = pd.DataFrame(solutions)
            sub_incidents_df = pd.DataFrame(sub_incidents)
            tags_df = pd.DataFrame(tags)

            self.__prints("Copying data to SQL")
            # Copy DF to SQL
            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", incident_df, "Incidents",
            )

            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", cc_df, "IncidentCC",
            )

            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", assets_df, "IncidentAssets",
            )

            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                associated_sla_names_df,
                "IncidentAssociatedSLA",
            )

            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                attachments_df,
                "IncidentAttachments",
            )
            data_types = {
                col_name: types.VARCHAR(length=255) for col_name in comments_df
            }
            data_types["body"] = types.VARCHAR(length=8000)
            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", comments_df, "IncidentComments",
            )

            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                sla_violations_df,
                "IncidentSLAViolations",
            )

            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                solutions_df,
                "IncidentSolutions",
            )

            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                sub_incidents_df,
                "IncidentSubIncidents",
            )

            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", tags_df, "IncidentTags",
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            if today_only:
                self.lc.execute_command(
                    self.__create_con(), "{ CALL ServiceDesk.pMoveIncidentsToday }"
                )
            else:
                self.lc.execute_command(
                    self.__create_con(), "{ CALL ServiceDesk.pMoveIncidents }"
                )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Incidents Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))

    def update_hardware(self, include_software=True, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Hardware")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Hardware")

        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:

            data = []

            link = "https://api.samanage.com/hardwares.json?per_page=100&layout=long"

            counter = 1

            headers = self.__get_headers()

            while True:

                self.__prints("Loading Page: {}".format(str(counter)))

                response = requests.get(link, headers=headers)

                data.extend(response.json())

                if "next" in response.links.keys():
                    link = response.links["next"]["url"]
                    counter += 1
                else:
                    break

            self.__prints("Loading Data Completed.")
            self.__prints("{} records loaded.".format(str(len(data))))

            bioses = {
                "device_id": [],
                "bios_id": [],
                "manufacturer": [],
                "model": [],
                "previous_ssn": [],
                "reported_at": [],
                "ssn": [],
                "version": [],
            }

            displays = {
                "device_id": [],
                "display_id": [],
                "description": [],
                "display_type": [],
                "manufacturer": [],
                "name": [],
                "reported_at": [],
                "serial_number": [],
            }

            controllers = {
                "device_id": [],
                "controller_id": [],
                "description": [],
                "type": [],
                "manufacturer": [],
                "name": [],
                "reported_at": [],
                "version": [],
            }

            drives = {
                "device_id": [],
                "drive_id": [],
                "drive_name": [],
                "drive_type": [],
                "free_space": [],
                "name": [],
                "reported_at": [],
                "total_space": [],
            }

            inputs = {
                "device_id": [],
                "input_id": [],
                "input_type": [],
                "manufacturer": [],
                "name": [],
                "reported_at": [],
            }

            memories = {
                "device_id": [],
                "memory_id": [],
                "capacity": [],
                "description": [],
                "memory_type": [],
                "name": [],
                "purpose": [],
                "reported_at": [],
                "slot": [],
                "speed": [],
            }

            networks = {
                "device_id": [],
                "network_id": [],
                "description": [],
                "dhcp": [],
                "gateway": [],
                "ip_address": [],
                "mac_address": [],
                "reported_at": [],
                "status": [],
            }

            ports = {
                "device_id": [],
                "port_id": [],
                "description": [],
                "name": [],
                "port_type": [],
                "reported_at": [],
            }

            printers = {
                "device_id": [],
                "printer_id": [],
                "address": [],
                "department": [],
                "driver": [],
                "name": [],
                "port": [],
                "shared": [],
                "site": [],
                "technical_contact": [],
            }

            sounds = {
                "device_id": [],
                "sound_id": [],
                "manufacturer": [],
                "name": [],
                "reported_at": [],
            }

            storages = {
                "device_id": [],
                "storage_id": [],
                "manufacturer": [],
                "model": [],
                "name": [],
                "reported_at": [],
                "size": [],
                "storage_type": [],
            }

            videos = {
                "device_id": [],
                "video_id": [],
                "chipset": [],
                "memory": [],
                "name": [],
                "reported_at": [],
            }

            device_data = {
                "id": [],
                "created": [],
                "updated": [],
                "active_directory": [],
                "asset_tag": [],
                "category_id": [],
                "cpu": [],
                "department_id": [],
                "description": [],
                "detected_at": [],
                "domain": [],
                "ip": [],
                "latitude": [],
                "longitude": [],
                "memory": [],
                "model": [],
                "name": [],
                "operating_system": [],
                "operating_system_version": [],
                "owner_id": [],
                "owner_group_id": [],
                "physical_memory": [],
                "processor_speed": [],
                "product_number": [],
                "site_id": [],
                "status": [],
                "swap": [],
                "username": [],
                "serial_number": [],
                "purchase_date": [],
                "purchase_number": [],
                "purchase_vendor": [],
                "category_name": [],
                "lifecycle_date": [],
            }

            software = {
                "device_id": [],
                "software_id": [],
                "first_detected": [],
            }

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            counter = 1
            for i in data:
                self.__prints(
                    "Starting: {} of {}.".format(str(counter), str(len(data)))
                )
                device_data["id"].append(i["id"])
                device_data["created"].append(i["created_at"])
                device_data["updated"].append(i["updated_at"])
                device_data["active_directory"].append(i["active_directory"])
                device_data["asset_tag"].append(i["asset_tag"])
                if i["category"] is None:
                    device_data["category_id"].append(None)
                    device_data["cageory_name"].append(None)
                else:
                    device_data["category_id"].append(i["category"]["id"])
                    device_data["category_name"].append(i["category"]["name"])
                device_data["cpu"].append(i["cpu"])
                if i["department"] is None:
                    device_data["department_id"].append(None)
                else:
                    device_data["department_id"].append(i["department"]["id"])
                device_data["description"].append(i["description"])
                device_data["detected_at"].append(i["detected_at"])
                device_data["domain"].append(i["domain"])
                device_data["ip"].append(i["ip"])
                device_data["latitude"].append(i["latitude"])
                device_data["longitude"].append(i["longitude"])
                device_data["memory"].append(i["memory"])
                device_data["model"].append(i["model"])
                device_data["name"].append(i["name"])
                device_data["operating_system"].append(i["operating_system"])
                device_data["operating_system_version"].append(
                    i["operating_system_version"]
                )
                if i["owner"] is None:
                    device_data["owner_id"].append(None)
                    device_data["owner_group_id"].append(None)
                else:
                    if i["owner"]["is_user"]:
                        device_data["owner_id"].append(i["owner"]["id"])
                    else:
                        device_data["owner_id"].append(None)
                    if "group_id" in i["owner"].keys():
                        device_data["owner_group_id"].append(i["owner"]["group_id"])
                    else:
                        device_data["owner_group_id"].append(None)

                device_data["physical_memory"].append(i["physical_memory"])
                device_data["processor_speed"].append(i["processor_speed"])
                device_data["product_number"].append(i["product_number"])
                if i["site"] is None:
                    device_data["site_id"].append(None)
                else:
                    device_data["site_id"].append(i["site"]["id"])
                device_data["status"].append(i["status"]["name"])
                device_data["swap"].append(i["swap"])
                device_data["username"].append(i["username"])
                device_data["serial_number"].append(i["serial_number"])

                if i["purchase"] is None:
                    device_data["purchase_date"].append(None)
                    device_data["purchase_number"].append(None)
                    device_data["purchase_vendor"].append(None)
                else:
                    device_data["purchase_date"].append(i["purchase"]["date"])
                    device_data["purchase_number"].append(i["purchase"]["number"])
                    if i["purchase"]["vendor"] is None:
                        device_data["purchase_vendor"].append(None)
                    else:
                        device_data["purchase_vendor"].append(
                            i["purchase"]["vendor"]["name"]
                        )

                lifecycle_date = None

                if len(i["custom_fields_values"]) > 0:
                    for value in i["custom_fields_values"]:
                        if value["custom_field_id"] == 118844:
                            lifecycle_date = datetime.strptime(
                                value["value"], "%b %d, %Y"
                            )

                device_data["lifecycle_date"].append(lifecycle_date)

                # Bioses
                for j in i["bioses"]:
                    bioses["device_id"].append(i["id"])
                    bioses["bios_id"].append(j["id"])
                    bioses["manufacturer"].append(j["manufacturer"])
                    bioses["model"].append(j["model"])
                    bioses["previous_ssn"].append(j["previous_ssn"])
                    bioses["reported_at"].append(j["reported_at"])
                    bioses["ssn"].append(j["ssn"])
                    bioses["version"].append(j["version"])

                # Controllers
                for j in i["controllers"]:
                    controllers["device_id"].append(i["id"])
                    controllers["controller_id"].append(j["id"])
                    controllers["description"].append(j["description"])
                    controllers["type"].append(j["controller_type"])
                    controllers["manufacturer"].append(j["manufacturer"])
                    controllers["name"].append(j["name"])
                    controllers["reported_at"].append(j["reported_at"])
                    controllers["version"].append(j["version"])

                # Displays
                for j in i["displays"]:
                    displays["device_id"].append(i["id"])
                    displays["display_id"].append(j["id"])
                    displays["description"].append(j["description"])
                    displays["display_type"].append(j["display_type"])
                    displays["manufacturer"].append(j["manufacturer"])
                    displays["name"].append(j["name"])
                    displays["reported_at"].append(j["reported_at"])
                    displays["serial_number"].append(j["serial_number"])

                # Drives
                for j in i["drives"]:
                    drives["device_id"].append(i["id"])
                    drives["drive_id"].append(j["id"])
                    drives["drive_name"].append(j["drive_name"])
                    drives["drive_type"].append(j["drive_type"])
                    drives["free_space"].append(j["free_space"])
                    drives["name"].append(j["name"])
                    drives["reported_at"].append(j["reported_at"])
                    drives["total_space"].append(j["total_space"])

                # Inputs
                for j in i["inputs"]:
                    inputs["device_id"].append(i["id"])
                    inputs["input_id"].append(j["id"])
                    inputs["input_type"].append(j["input_type"])
                    inputs["manufacturer"].append(j["manufacturer"])
                    inputs["name"].append(j["name"])
                    inputs["reported_at"].append(j["reported_at"])

                # Memories
                for j in i["memories"]:
                    memories["device_id"].append(i["id"])
                    memories["memory_id"].append(j["id"])
                    memories["capacity"].append(j["capacity"])
                    memories["description"].append(j["description"])
                    memories["memory_type"].append(j["memory_type"])
                    memories["name"].append(j["name"])
                    memories["purpose"].append(j["purpose"])
                    memories["reported_at"].append(j["reported_at"])
                    memories["slot"].append(j["slot"])
                    memories["speed"].append(j["speed"])

                # Networks
                for j in i["networks"]:
                    networks["device_id"].append(i["id"])
                    networks["network_id"].append(j["id"])
                    networks["description"].append(j["description"])
                    networks["dhcp"].append(j["dhcp"])
                    networks["gateway"].append(j["gateway"])
                    networks["ip_address"].append(j["ip_address"])
                    networks["mac_address"].append(j["mac_address"])
                    networks["reported_at"].append(j["reported_at"])
                    networks["status"].append(j["status"])

                # Ports
                if i["ports"] is not None:
                    for j in i["ports"]:
                        ports["device_id"].append(i["id"])
                        ports["port_id"].append(j["id"])
                        ports["description"].append(j["description"])
                        ports["name"].append(j["name"])
                        ports["port_type"].append(j["port_type"])
                        ports["reported_at"].append(j["reported_at"])

                # Printers
                if i["printers"] is not None:
                    for j in i["printers"]:
                        printers["device_id"].append(i["id"])
                        printers["printer_id"].append(j["id"])
                        printers["address"].append(j["address"])
                        printers["department"].append(j["department"])
                        printers["driver"].append(j["driver"])
                        printers["name"].append(j["name"])
                        printers["port"].append(j["port"])
                        printers["shared"].append(j["shared"])
                        printers["site"].append(j["site"])
                        printers["technical_contact"].append(j["technical_contact"])

                # Sounds
                if i["sounds"] is not None:
                    for j in i["sounds"]:
                        sounds["device_id"].append(i["id"])
                        sounds["sound_id"].append(j["id"])
                        sounds["manufacturer"].append(j["manufacturer"])
                        sounds["name"].append(j["name"])
                        sounds["reported_at"].append(j["reported_at"])

                # Storages
                if i["storages"] is not None:
                    for j in i["storages"]:
                        storages["device_id"].append(i["id"])
                        storages["storage_id"].append(j["id"])
                        storages["manufacturer"].append(j["manufacturer"])
                        storages["model"].append(j["model"])
                        storages["name"].append(j["name"])
                        storages["reported_at"].append(j["reported_at"])
                        storages["size"].append(j["size"])
                        storages["storage_type"].append(j["storage_type"])

                # Videos
                if i["videos"] is not None:
                    for j in i["videos"]:
                        videos["device_id"].append(i["id"])
                        videos["video_id"].append(j["id"])
                        videos["chipset"].append(j["chipset"])
                        videos["memory"].append(j["memory"])
                        videos["name"].append(j["name"])
                        videos["reported_at"].append(j["reported_at"])

                if i["softwares_href"] is not None and include_software:
                    software_link = i["softwares_href"] + "?page={}per_page=100"

                    sf_data = []

                    page = 1

                    try:
                        while True:
                            response = requests.get(
                                software_link.format(str(page)), headers=headers
                            )

                            total_pages = int(response.headers["X-Total-Pages"])

                            sf_data.extend(response.json())

                            self.__prints(
                                "Software Page {} of {} Loaded.".format(
                                    str(page), str(total_pages)
                                )
                            )

                            if page < total_pages:
                                page += 1
                            else:
                                break

                        for j in sf_data:
                            software["device_id"].append(i["id"])
                            software["software_id"].append(j["id"])
                            software["first_detected"].append(j["first_detected"])
                    except Exception as e:
                        self.__prints(str(e))

                self.__prints(
                    "Completed: {} of {}.".format(str(counter), str(len(data)))
                )
                counter += 1

            device_df = pd.DataFrame(device_data)
            bioses_df = pd.DataFrame(bioses)
            controllers_df = pd.DataFrame(controllers)
            displays_df = pd.DataFrame(displays)
            drives_df = pd.DataFrame(drives)
            inputs_df = pd.DataFrame(inputs)
            memories_df = pd.DataFrame(memories)
            networks_df = pd.DataFrame(networks)
            ports_df = pd.DataFrame(ports)
            printers_df = pd.DataFrame(printers)
            sounds_df = pd.DataFrame(sounds)
            storages_df = pd.DataFrame(storages)
            videos_df = pd.DataFrame(videos)
            software_df = pd.DataFrame(software)

            self.__prints("Copying data to SQL")

            # Copy DataFrame to SQL Server
            data_type = {col_name: types.VARCHAR(length=255) for col_name in device_df}
            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", device_df, "Devices", data_type,
            )

            data_type = {col_name: types.VARCHAR(length=255) for col_name in bioses_df}
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                bioses_df,
                "DeviceBios",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in controllers_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                controllers_df,
                "DeviceControllers",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in displays_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                displays_df,
                "DeviceDisplays",
                data_type,
            )

            data_type = {col_name: types.VARCHAR(length=255) for col_name in drives_df}
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                drives_df,
                "DeviceDrives",
                data_type,
            )

            data_type = {col_name: types.VARCHAR(length=255) for col_name in inputs_df}
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                inputs_df,
                "DeviceInputs",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in memories_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                memories_df,
                "DeviceMemory",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in networks_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                networks_df,
                "DeviceNetwork",
                data_type,
            )

            data_type = {col_name: types.VARCHAR(length=255) for col_name in ports_df}
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                ports_df,
                "DevicePorts",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in printers_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                printers_df,
                "DevicePrinters",
                data_type,
            )

            data_type = {col_name: types.VARCHAR(length=255) for col_name in sounds_df}
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                sounds_df,
                "DeviceSounds",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in storages_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                storages_df,
                "DeviceStorage",
                data_type,
            )

            data_type = {col_name: types.VARCHAR(length=255) for col_name in videos_df}
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                videos_df,
                "DeviceVideo",
                data_type,
            )

            if include_software:
                data_type = {
                    col_name: types.VARCHAR(length=255) for col_name in software_df
                }
                self.lc.df_to_sql(
                    self.__create_pd_con(),
                    "ServiceDesk",
                    software_df,
                    "DeviceSoftware",
                    data_type,
                )

            self.__prints("Moving data from stage")

            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveDevices }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Hardware Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))

    def update_groups(self, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Groups")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Groups")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:

            data = []

            link = "https://api.samanage.com/groups.json?per_page=100"

            page_number = 1

            while True:
                self.__prints(f"Loading Page {page_number}")
                response = requests.get(link, headers=self.__get_headers())

                data.extend(response.json())

                if (
                    "next" in response.links.keys()
                    and response.links["next"]["url"] != link
                ):
                    link = response.links["next"]["url"]
                else:
                    break

            groups = {
                "id": [],
                "name": [],
                "description": [],
                "disabled": [],
                "email": [],
                "is_user": [],
                "reports_to": [],
            }

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            for i in data:
                groups["id"].append(i["id"])
                groups["name"].append(i["name"])
                groups["description"].append(None)
                groups["disabled"].append(i["disabled"])
                groups["email"].append(None)
                groups["is_user"].append(i["is_user"])
                groups["reports_to"].append(i["reports_to"]["id"])

            groups_df = pd.DataFrame(groups)

            self.__prints("Copying data to SQL")
            # Copy DF to SQL
            data_type = {col_name: types.VARCHAR(length=255) for col_name in groups_df}
            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", groups_df, "Groups", data_type,
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveGroups }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Groups Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))

    def update_departments(self, print_status=True):

        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Departments")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Departments")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:
            data = []

            link = "https://api.samanage.com/departments.json?per_page=100"

            page_number = 1

            while True:
                self.__prints(f"Loading Page {page_number}")
                response = requests.get(link, headers=self.__get_headers())

                data.extend(response.json())

                if "next" in response.links.keys():
                    link = response.links["next"]["url"]
                else:
                    break

            departments = {
                "id": [],
                "name": [],
                "default_assignee_id": [],
                "description": [],
            }

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            for i in data:
                departments["id"].append(i["id"])
                departments["name"].append(i["name"])
                departments["default_assignee_id"].append(i["default_assignee_id"])
                departments["description"].append(i["description"])

            departments_df = pd.DataFrame(departments)

            self.__prints("Copying data to SQL")
            # Copy DF to SQL
            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in departments_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                departments_df,
                "Departments",
                data_type,
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveDepartments }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Departments Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))

    def update_changes(self, print_status=True):
        self.print_status = print_status

        self.__prints("")
        self.__prints("Update ServiceDesk Changes")
        self.__prints("")

        # Start Log
        log_id = create_log(self.__create_con(), task="ServiceDesk Changes")
        self.__prints(f"Logging with LogID: {log_id}")
        self.__prints("")

        try:

            data = []

            link = "https://api.samanage.com/changes.json?per_page=100&layout=long"

            counter = 1

            while True:

                self.__prints("Loading Page: {}".format(str(counter)))

                response = requests.get(link, headers=self.__get_headers())

                data.extend(response.json())

                if "next" in response.links.keys():
                    link = response.links["next"]["url"]
                    counter += 1
                else:
                    break

            self.__prints("Loading Data Completed.")
            self.__prints("{} records loaded.".format(str(len(data))))

            changes = {
                "id": [],
                "assignee_id": [],
                "approve_denied_at": [],
                "approve_requested_at": [],
                "approved_at": [],
                "change_plan": [],
                "created_at": [],
                "created_by_id": [],
                "department_id": [],
                "description": [],
                "description_no_html": [],
                "name": [],
                "number": [],
                "planned_end_at": [],
                "planned_start_at": [],
                "priority": [],
                "release_id": [],
                "requester_id": [],
                "rollback_plan": [],
                "site_id": [],
                "state": [],
                "test_plan": [],
                "updated_at": [],
            }

            approvers = {"change_id": [], "approver_id": []}

            attachments = {"change_id": [], "filename": [], "secure_url": []}

            self.__prints("")
            self.__prints("Processing Data")
            self.__prints("")

            counter = 1
            for i in data:
                self.__prints(
                    "Starting: {} of {}.".format(str(counter), str(len(data)))
                )
                changes["id"].append(i["id"])
                if i["assignee"] is None:
                    changes["assignee_id"].append(None)
                else:
                    changes["assignee_id"].append(i["assignee"]["id"])
                changes["approve_denied_at"].append(i["approve_denied_at"])
                changes["approve_requested_at"].append(i["approve_requested_at"])
                changes["approved_at"].append(i["approved_at"])
                changes["change_plan"].append(i["change_plan"])
                changes["created_at"].append(i["created_at"])
                if i["created_by"] is None:
                    changes["created_by_id"].append(None)
                else:
                    changes["created_by_id"].append(i["created_by"]["id"])
                if i["department"] is None:
                    changes["department_id"].append(None)
                else:
                    changes["department_id"].append(i["department"]["id"])
                changes["description"].append(i["description"])
                changes["description_no_html"].append(i["description_no_html"])
                changes["name"].append(i["name"])
                changes["number"].append(i["number"])
                changes["planned_end_at"].append(i["planned_end_at"])
                changes["planned_start_at"].append(i["planned_start_at"])
                changes["priority"].append(i["priority"])
                if "realease" not in i:
                    changes["release_id"].append(None)
                elif i["release"] is None:
                    changes["release_id"].append(None)
                else:
                    changes["release_id"].append(i["release"]["id"])
                if i["requester"] is None:
                    changes["requester_id"].append(None)
                else:
                    changes["requester_id"].append(i["requester"]["user_id"])
                changes["rollback_plan"].append(i["rollback_plan"])
                if i["site"] is None:
                    changes["site_id"].append(None)
                else:
                    changes["site_id"].append(i["site"]["id"])
                changes["state"].append(i["state"])
                changes["test_plan"].append(i["test_plan"])
                changes["updated_at"].append(i["updated_at"])

                for j in i["attachments"]:
                    attachments["change_id"].append(i["id"])
                    attachments["filename"].append(j["filename"])
                    attachments["secure_url"].append(j["secure_url"])

                if i["approvers"] is not None:
                    for j in i["approvers"]:
                        approvers["change_id"].append(i["id"])
                        approvers["approver_id"].append(j["id"])

                self.__prints(
                    "Completed: {} of {}.".format(str(counter), str(len(data)))
                )
                counter += 1

            change_df = pd.DataFrame(changes)
            attachments_df = pd.DataFrame(attachments)
            approvers_df = pd.DataFrame(approvers)

            self.__prints("Copying data to SQL")
            data_type = {col_name: types.VARCHAR(length=255) for col_name in change_df}
            data_type["description"] = types.VARCHAR(length=8000)
            data_type["description_no_html"] = types.VARCHAR(length=8000)
            self.lc.df_to_sql(
                self.__create_pd_con(), "ServiceDesk", change_df, "Changes", data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in attachments_df
            }

            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                attachments_df,
                "ChangesAttachments",
                data_type,
            )

            data_type = {
                col_name: types.VARCHAR(length=255) for col_name in approvers_df
            }
            self.lc.df_to_sql(
                self.__create_pd_con(),
                "ServiceDesk",
                approvers_df,
                "ChangesApprovers",
                data_type,
            )

            self.__prints("Moving data from stage")
            # Move data from Stage to SourceData
            self.lc.execute_command(
                self.__create_con(), "{ CALL ServiceDesk.pMoveChanges }"
            )

            self.__prints("")
            self.__prints("Completing Log")
            # Complete log
            create_log(self.__create_con(), log_id, status="success")

            self.__prints("")
            self.__prints("Update ServiceDesk Changes Completed")

        except Exception as e:
            self.__prints(f"Error Message: {str(e)}")
            create_log(self.__create_con(), log_id, status="failure", message=str(e))
 