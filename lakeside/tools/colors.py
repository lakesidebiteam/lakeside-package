def rgba_to_hex(r, g, b, a=255):
    return "#{:02x}{:02x}{:02x}{:02x}".format(
        r,
        g,
        b,
        a,
    )
