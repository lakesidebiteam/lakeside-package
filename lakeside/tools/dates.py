import pandas as pd
import pytz
import dateutil


def convert_str_utc_pt_str(datetime, time_zone=None) -> str:
        """Converts string utc time to local time and returns it as a string

        Keyword Arguments:
        datetime - datetime object to be converted
        time_zone - timezone for date to be converted to (Default None is US/Pacific)
        """

        if pd.isnull(datetime):
            return None

        datetime = dateutil.parser.parse(datetime)

        if datetime.tzinfo is None and datetime.tzinfo.utcoffset(datetime) is None:
            datetime = pytz.utc.localize(datetime)

        if time_zone is None:
            time_zone = dateutil.tz.gettz("US/Pacific")

        conv_dt = datetime.astimezone(time_zone)
        return str(conv_dt)[:19]

def time_to_seconds(time):
    hr_sec = time.hour * 60 * 60
    min_sec = time.minute * 60
    return hr_sec + min_sec + time.second