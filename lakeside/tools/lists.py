def first_item(data: list):
    """Returns the first item of a list. If the list is empty it returns None"""
    if data is None:
        return data
    elif len(data) == 0:
        return None
    else:
        return data[0]