import pkg_resources
from os import listdir
from os.path import join

def resource_dir():
    return pkg_resources.resource_filename('lakeside', 'resources/')

def resource_list():
    exclude_files = ['__init__.py', '__pycache__']

    return [f for f in listdir(resource_dir()) if f not in exclude_files]

def get_resource_path(resouce):
    if resouce not in resource_list():
        return None
    else:
        return join(resource_dir(), resouce)