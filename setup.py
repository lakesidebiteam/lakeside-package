import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="lakeside",
    version="0.0.1",
    author="Aaron Predmore",
    author_email="aaron.predmore@lakesideindustries.com",
    description="Commonly used stuff",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/Aaron-Lakeside/Lakeside-Package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.7",
    install_requires=[
        "ldap3",
        "pandas",
        "pypyodbc",
        "pyodbc",
        "sqlalchemy",
        "flask",
        "phonenumbers",
        "requests",
        "mygeotab",
        "xmltodict",
        "xmltodict",
        "beautifulsoup4",
        "msal",
    ],
    package_data={'lakeside': ['resources/*.png']}
)